package G8R.app;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 3
 * Class: CSI 4321
 *
 ************************************************/
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import G8R.serialization.*;

/**
 * Handles the client and repeatedly receives the requests and sends responses
 * while logging the messages
 * @author Clint Masters
 * @version 1.0
 */
public class ClientHandler {
	// Indicates invalid request and the status is ERROR.
	private static final String ERROR = "ERROR";
	// Indicates the end of the sequence of G8RMessages
	private static final String NULL = "NULL";
	private Socket clntSock;
	private Logger logger;

	private State state;

	/**
	 * Constructs a client handler
	 * @param clntSock the client socket for this handler
	 * @param logger to log the messages and errors
	 */
	public ClientHandler(Socket clntSock, Logger logger) {
		this.clntSock = clntSock;
		this.logger = logger;
		state=State.Initial;
	}

	/**
	 * handles the G8RClient by setting the states and logging messages
	 * @param threadID of the thread calling
	 * @throws ValidationException if invalid token
	 */
	public void handleG8RClient(Long threadID) throws ValidationException  {
		try {
			MessageInput in = new MessageInput(clntSock.getInputStream());
			MessageOutput out = new MessageOutput(clntSock.getOutputStream());
			G8RRequest request;
			G8RResponse response = new G8RResponse();
			while(getState()!=null) {
				try {
					request = (G8RRequest)G8RMessage.decode(in);
					state = state.respond(request, out, response);
					logInfo(response, request, threadID);

				} catch (ValidationException e) {
					// Send error response if invalid Request
					state.sendResponse(ERROR, NULL, e.getMessage(), out, new CookieList(), response);
					logger.log(Level.WARNING, "***client terminated"+System.lineSeparator());
					setState(null);

				} catch (IOException i) {
					logger.log(Level.WARNING, "***client terminated"+System.lineSeparator());
					setState(null);
				}
			} 
		} catch (NullPointerException | IOException e) {
			System.err.print(e.getMessage()+"\n");
		}	
	}

	/**
	 * Returns the state
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * Changes the state
	 * @param p the new state
	 */
	public void setState(State p) {
		state=p;
	}

	/**
	 * Logs the response and request message
	 * @param response to be logged
	 * @param request to be logged
	 * @param threadID of calling thread
	 */
	public void logInfo(G8RResponse response, G8RRequest request, Long threadID) {
		logger.log(Level.INFO, clntSock.getInetAddress()+":"+clntSock.getPort()+
				"-"+threadID+" [Received: "+request+"|Sent: "+response+"]"+
				System.lineSeparator());
	}
}
