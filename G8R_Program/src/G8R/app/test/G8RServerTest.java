package G8R.app.test;
/************************************************
*
* Author: Clint Masters
* Assignment: Program 3
* Class: CSI 4321
*
************************************************/
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;

import java.net.Socket;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import G8R.app.G8RServer;
import G8R.app.State;
import G8R.serialization.*;

/**
 * Some hard coded clients to test the server
 * @author Clint Masters
 *
 */
class G8RServerTest {
	/**
	 * Create the server for the clients to use
	 * @throws InterruptedException if interrupted
	 */
	@BeforeAll
	public static void server() throws InterruptedException {
		Thread serverThread = new Thread(() -> {
		String[] params = {"12345", "1"};
		try {
			G8RServer.main(params);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		});
		serverThread.start();
	}
	
	/**
	 * Test the happy route of Poll
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void PollSuccess() throws IOException, ValidationException {
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
        MessageInput in = new MessageInput(clnt.getInputStream());
        CookieList cookies=new CookieList();
        G8RRequest request = new G8RRequest("Poll", new String[0],cookies);
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        assertEquals(State.NameStep.name(),response.getFunction());
        clnt.close();
	}
	
	/**
	 * Test the happy route of RPS
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void RPSSuccess() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("RPS", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        assertEquals(State.RPSResult.name(),response.getFunction());
        clnt.close();
	}
	
	/**
	 * Test the route where there is an error and then success
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void RPSFailureToSuccess() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("RPS", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction(response.getFunction());
        request.setParams(new String[] {"Scioor"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        request.setParams(new String[] {"Scissors"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("OK",response.getStatus());
        clnt.close();
	}
	
	/**
	 * Test the route where there is an invalid function
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void FunctionFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("Failure", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Unexpected Function",response.getMessage());
        clnt.close();
	}
	
	/**
	 * Test the route with incorrect namestep function
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void NameStepFunctionFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("Poll", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction("TestFailNameStep");
        request.setParams(new String[] {"Clint","Masters"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Unexpected Function",response.getMessage());
        clnt.close();
	}
	
	/**
	 * Test incorrect foodstep function
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void FoodStepFunctionFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("Poll", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction(response.getFunction());
        request.setParams(new String[] {"Clint","Masters"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction("NOTFOODSTEP");
        request.setParams(new String[] {"Mexican"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Unexpected Function",response.getMessage());
        clnt.close();
	}
	
	/**
	 * Test incorrect rps function
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void RPSResultFunctionFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("RPS", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction("TESTFAILRPS");
        request.setParams(new String[] {"Rock"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Unexpected Function",response.getMessage());
        clnt.close();
	}
	
	/**
	 * Test invalid parameters for RPS
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void RPSResultBadParamFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("RPS", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction(response.getFunction());
        request.setParams(new String[] {"Rock","Test"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Invalid Parameter: Rock, Paper, or Scissors?>",response.getMessage());
        clnt.close();
	}
	
	/**
	 * Test invalid parameters for NameStep
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void NameStepBadParamFailure() throws IOException, ValidationException {	
		Socket clnt = new Socket("localhost",12345);
		MessageOutput out = new MessageOutput(clnt.getOutputStream());
		MessageInput in = new MessageInput(clnt.getInputStream());
        G8RRequest request = new G8RRequest("Poll", new String[0],new CookieList());
        request.encode(out);
        G8RResponse response = (G8RResponse)G8RMessage.decode(in);
        request.setCookieList(response.getCookieList());
        request.setFunction(response.getFunction());
        request.setParams(new String[] {"Clint","Masters","TEST"});
        request.encode(out);
        response = (G8RResponse)G8RMessage.decode(in);
        assertEquals("ERROR",response.getStatus());
        assertEquals("Poorly formed name. Name (First Last)>",response.getMessage());
        clnt.close();
	}

}
