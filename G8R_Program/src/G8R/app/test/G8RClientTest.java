package G8R.app.test;
/************************************************
*
* Author: Clint Masters
* Assignment: Program 2
* Class: CSI 4321
*
************************************************/
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import G8R.app.G8RClient;
import G8R.serialization.*;
/**
 * Tests Connecting to server and several various scenarios for G8RClient
 * @version 1.0
 * @author Clint Masters
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class G8RClientTest {

	/**
	 * Creates the server
	 * @throws IOException if I/O problem
	 */
	@BeforeAll
	public static void serverTest() throws IOException {
		Thread serverThread = new Thread(() -> {
			ServerSocket servSock;
			try {
				int testNum=0;
				String[] responseFiles = {"out0.txt","out1.txt"};
				servSock = new ServerSocket(12345);
				while (true) { 
					// Run forever, accepting and servicing connections
					Socket clntSock = servSock.accept();     
					// Get client connection
					BufferedReader in = new BufferedReader(
							new InputStreamReader(clntSock.getInputStream()));
					OutputStream out = clntSock.getOutputStream();
					FileInputStream fIn = new FileInputStream(
							responseFiles[testNum]);	
					MessageInput mIn = new MessageInput(fIn);
					G8RResponse testRep = (G8RResponse)G8RMessage.decode(mIn);
					while (in.readLine()!=null){
						testRep.encode(new MessageOutput(out));
						if(fIn.available()!=0) {
							testRep = (G8RResponse)G8RMessage.decode(mIn);
						}
					}

					clntSock.close();
					testNum++;
				}

			} catch (IOException | NullPointerException | 
					ValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		serverThread.start();
	}

	/**
	 * Tests G8RMessage being passed with cookies
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void testClient1ValidCookies() throws IOException, ValidationException {
		String[] params = {"localhost", "12345", "test1.txt"};
		System.setIn(new ByteArrayInputStream("Gu\nes\ns".getBytes()));
		PrintStream p = System.out;
		System.setOut(new PrintStream(new BufferedOutputStream(
				new FileOutputStream("expect0.txt")),true));
		G8RClient.main(params);
		System.setOut(p);
		Scanner fIn = new Scanner(new File("expect0.txt"));
		String testStr = fIn.nextLine();
		assertTrue(testStr.contains("END OF TEST2"));
		fIn.close();
	}
	
	/**
	 * Tests valid message with no cookies
	 * @throws IOException if I/O problem
	 * @throws ValidationException if invalid token
	 */
	@Test
	void testClient2ValidNoCookies() throws IOException, ValidationException {
		String[] params = {"localhost", "12345", "test.txt"};
		System.setIn(new ByteArrayInputStream("Test\nTest\nTest".getBytes()));
		PrintStream p = System.out;
		System.setOut(new PrintStream(new BufferedOutputStream(
				new FileOutputStream("expect1.txt")),true));
		G8RClient.main(params);
		System.setOut(p);
		Scanner fIn = new Scanner(new File("expect1.txt"));
		String testStr = fIn.nextLine();
		assertTrue(testStr.contains("Function> Name"));
		fIn.close();
	}

}
