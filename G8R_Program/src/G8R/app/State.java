package G8R.app;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 3
 * Class: CSI 4321
 *
 ************************************************/
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import G8R.serialization.*;

/**
 * This enumerated class serves as the state in the state design pattern and after each state is 
 * called it returns the correct next state
 * @author Clint Masters
 *
 */
public enum State {
	/**
	 * Validates the function and chooses which application to run
	 */
	Initial{
		@Override
		State respond(G8RRequest request, MessageOutput out, G8RResponse response) throws IOException, 
		ValidationException {
			if("Poll".equals(request.getFunction())) {
				// If users name is already known start at NameStep
				if(checkForNames(request.getCookieList())){
					request.setFunction(NameStep.name());
					return NameStep.respond(request, out, response);
				}
				sendResponse(OK,NameStep.name(),"Name (First Last)>",out, request.getCookieList(), response);
				return NameStep;
			}
			else if("RPS".equals(request.getFunction())) {
				CookieList cookieList = request.getCookieList();
				createResults(cookieList);
				sendResponse(OK, RPSResult.name(), "Rock, Paper, or Scissors?>",out,cookieList,response);
				return RPSResult;
			}
			sendResponse(ERROR, NULL, "Unexpected Function",out,request.getCookieList(),response);
			return null;
		}
	}, 
	/**
	 * Gets the users name for the Poll application
	 */
	NameStep{
		@Override
		State respond(G8RRequest request, MessageOutput out, G8RResponse response) throws ValidationException,
		IOException {
			CookieList cookieList = new CookieList(request.getCookieList());
			if(NameStep.name().equals(request.getFunction())) {
				if(!checkForNames(cookieList)) {
					if(request.getParams().length!=2) {
						sendResponse(ERROR,NameStep.name(),"Poorly formed name. Name (First Last)>", out,
								request.getCookieList(), response);
						return NameStep;
					}
					if(request.getParams().length==2) {
						cookieList.add(FNAME, request.getParams()[0]);
						cookieList.add(LNAME, request.getParams()[1]);
					}
				}
				String message = cookieList.getValue(FNAME)+"'s Food mood>";
				sendResponse(OK, FoodStep.name(),message,out,cookieList, response);
				return FoodStep;
			}
			sendResponse(ERROR, NULL, "Unexpected Function",out,request.getCookieList(),response);
			return null;
		}
	}, 
	/**
	 * Gets and returns the users discount
	 */
	FoodStep{
		@Override
		State respond(G8RRequest request, MessageOutput out, G8RResponse response) throws ValidationException,
		IOException {
			if("FoodStep".equals(request.getFunction())) {
				CookieList cookieList = request.getCookieList();
				if(!cookieList.getNames().contains(REPEAT)) {
					cookieList.add(REPEAT, "0");
				}
				cookieList.add(REPEAT, (Integer.parseInt(cookieList.
						getValue(REPEAT))+1)+"");
				if(request.getParams().length!=1) {
					sendResponse(ERROR,FoodStep.name(),"Poorly formed Food "
							+ "mood. "+cookieList.getValue(FNAME)+"'s Food mood>", out,
							request.getCookieList(), response);
					return FoodStep;
				}
				String discount = getDiscount(request.getParams()[0],cookieList);
				sendResponse(OK, NULL,discount, out, cookieList, response);
				return null;
			}
			sendResponse(ERROR, NULL, "Unexpected Function",out,request.getCookieList(),response);
			return null;
		}
	},  
	/**
	 * Does the Result of the rock paper scissors game
	 */
	RPSResult{
		@Override
		State respond(G8RRequest request, MessageOutput out, G8RResponse response)
				throws ValidationException, IOException {
			if(RPSResult.name().equals(request.getFunction())) {
				String userChoice="";
				try {
					// If they entered the right number of choice get users choice
					if(request.getParams().length==1) {
						userChoice=request.getParams()[0];
					}
				} catch(NullPointerException e) {
					sendResponse(ERROR, RPSResult.name(), "Invalid Parameter: Rock, Paper, or Scissors?>",out,
							request.getCookieList(),response);
					return RPSResult;
				}
				// Validate that choice was correct
				if(ROCK.equals(userChoice)||SCISSORS.equals(userChoice)||PAPER.equals(userChoice)){
					CookieList cookieList = request.getCookieList();
					String message = getResults(userChoice, cookieList);	
					sendResponse(OK, NULL,message, out, cookieList, response);
					return null;
				}
				sendResponse(ERROR, RPSResult.name(), "Invalid Parameter: Rock, Paper, or Scissors?>",out,
						request.getCookieList(),response);
				return RPSResult;
			}
			sendResponse(ERROR, NULL, "Unexpected Function",out,request.getCookieList(),response);
			return null;
		}
	};

	// Indicates request was valid and status is OK.
	private static final String OK = "OK";
	// Indicates invalid request and the status is ERROR.
	private static final String ERROR = "ERROR";
	// Indicates the end of the sequence of G8RMessages
	private static final String NULL = "NULL";
	// Name of Cookie to set first name
	private static final String FNAME = "FName";
	// Name of Cookie to set last name
	private static final String LNAME = "LName";
	// Name of cookie for number of wins
	private static final String WINS = "Wins";
	// Name of cookie for number of ties
	private static final String TIES = "Ties";
	// Name of Cookie for the number of losses
	private static final String LOSSES = "Losses";
	// Name of cookie for the times the user has done the Poll
	private static final String REPEAT = "Repeat";
	// Choice for Rock in the RPS game
	private static final String ROCK = "Rock";
	// Choice for Paper in the RPS game
	private static final String PAPER = "Paper";
	// Choice for Scissors in the RPS game
	private static final String SCISSORS = "Scissors";

	/**
	 * Determines how to respond to the clients request
	 * @param request the clients request
	 * @param out the output stream for the response
	 * @param response G8RResponse to be sent to client
	 * @return the next State
	 * @throws ValidationException if invalid token
	 * @throws IOException if I/O Problem
	 */
	abstract State respond(G8RRequest request,
			MessageOutput out, G8RResponse response) throws ValidationException, IOException;

	/**
	 * Checks if the cookielist has names already
	 * @param cookieList of the request
	 * @return true if the cookielist contains first and last name
	 */
	boolean checkForNames(CookieList cookieList) {
		return (cookieList.getNames().containsAll(new 
				HashSet<String>(Arrays.asList(FNAME,LNAME))));
	}

	/**
	 * Send G8RResponse to client
	 * @param status status of the response
	 * @param function function of the response
	 * @param message message of the response
	 * @param out output stream to write response to
	 * @param cookieList cookielist of response
	 * @param response G8RResponse for which to set parameters
	 * @throws ValidationException if invalid token
	 * @throws IOException if I/O problem
	 */
	void sendResponse(String status, String function, String message, MessageOutput out,
			CookieList cookieList, G8RResponse response) throws ValidationException, IOException {
		response.setStatus(status);
		response.setCookieList(cookieList);
		response.setFunction(function);
		response.setMessage(message);
		response.encode(out);
	}

	/**
	 * Returns the discount for the Poll application
	 * @param choice the users food choice
	 * @param cookieList the cookielist for the request
	 * @return a string with the discount
	 */
	String getDiscount(String choice, CookieList cookieList) {
		switch(choice) {
		case "Italian":
			return "25% + " + cookieList.getValue(REPEAT)+"% off at Pastastic";
		case "Mexican":
			return "20% + " + cookieList.getValue(REPEAT)+"% off at Tacopia";
		default:
			return "10% + " + cookieList.getValue(REPEAT)+"% off at McDonalds";
		}
	}

	/**
	 * Returns the result of the rock paper scissors game
	 * @param userChoice the users choice
	 * @param cookieList the cookielist of the response
	 * @return the results of the game
	 * @throws ValidationException if invalid token
	 */
	String getResults(String userChoice,
			CookieList cookieList) throws ValidationException{
		String message = "";
		Random randNum = new Random();
		int compChoice = randNum.nextInt(3);
		String result = "";
		switch(compChoice) {
		case 0:
			if(ROCK.equals(userChoice)) {
				message += "The Computer chose rock! Tie Game!";
				result=TIES;
				break;
			} 
			else if(PAPER.equals(userChoice)) {
				message += "The Computer chose rock! You Win!";
				result=WINS;
				break;
			}
			message += "The Computer chose rock! You Lose!";
			result=LOSSES;
			break;
		case 1:
			if(ROCK.equals(userChoice)) {
				message += "The Computer chose paper! You Lose!";
				result=LOSSES;
				break;
			} 
			else if(PAPER.equals(userChoice)) {
				message += "The Computer chose paper! You Tie!";
				result=TIES;
				break;
			}
			message += "The Computer chose paper! You Win!";
			result=WINS;
			break;
		default:
			if(ROCK.equals(userChoice)) {
				message += "The Computer chose scissors! You Win!";
				result=WINS;
				break;
			} 
			else if(PAPER.equals(userChoice)) {
				message += "The Computer chose scissors! You Lose!";
				result=LOSSES;
				break;
			}
			message += "The Computer chose scissors! You Tie!";
			result=TIES;
		}
		cookieList.add(result, (Integer.parseInt(cookieList.
				getValue(result))+1)+"");
		return message+" Wins: "+cookieList.getValue(WINS)+" Losses: "+cookieList.getValue(LOSSES)+
				" Ties: "+ cookieList.getValue(TIES);
	}

	/**
	 * Adds the result cookies if they do not exist
	 * @param cookieList the cookielist to add to
	 * @throws ValidationException if invalid token
	 */
	void createResults(CookieList cookieList) throws ValidationException {
		if(!cookieList.getNames().contains(WINS)) {
			cookieList.add(WINS, "0");
		}
		if(!cookieList.getNames().contains(TIES)) {
			cookieList.add(TIES, "0");
		}
		if(!cookieList.getNames().contains(LOSSES)) {
			cookieList.add(LOSSES, "0");
		}
	}

}
