package G8R.app;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 3
 * Class: CSI 4321
 *
 ************************************************/
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.*;

import G8R.serialization.ValidationException;

/**
 * This server handles G8RRequests from clients and responds appropriately
 * @author Clint Masters
 * @version 1.0
 */
public class G8RServer {	
	// 20 seconds to timeout as specified
	private static final int TIMEOUT = 20000;
	/**
	 * Creates the server and the threads and continuously connects with 
	 * clients
	 * @param args the command line arguments
	 * @throws InterruptedException 
	 * @throws IOException if I/O problem
	 */
	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) { // Test for correct # of args
			throw new IllegalArgumentException("Parameter(s): <Port> <Threads>");
		}

		int servPort = Integer.parseInt(args[0]); // Server port
		int threadPoolSize = Integer.parseInt(args[1]);
		if(threadPoolSize<1) {
			throw new IllegalArgumentException("Must be at least 1 thread to receive clients");
		}

		// Create a server socket to accept client connection requests		
		try {
			try {
				final Logger logger = Logger.getLogger("connection");
				final ServerSocket servSock = new ServerSocket();
				servSock.setReuseAddress(true);
				servSock.bind(new InetSocketAddress(servPort));
				FileHandler fh;
				fh = new FileHandler("connections.log");
				logger.addHandler(fh);
				SimpleFormatter formatter = new SimpleFormatter();  
				fh.setFormatter(formatter); 
				logger.setUseParentHandlers(false);

				// Spawn a fixed number of threads to service clients
				for (int i = 0; i < threadPoolSize-1; i++) {
					clientController(logger,servSock);
				}
				Thread thread = clientController(logger,servSock);
				thread.join();
				servSock.close();
			} catch(IllegalArgumentException e) {
				System.err.print("Invalid Port\n");
			}
		} catch (IOException e1) {
			System.err.print(e1.getMessage()+"\n");
		} 
	}
	
	/**
	 * Controls the threads for the client sockets
	 * @param logger logs messages from the connections
	 * @param servSock the servers socket
	 * @return the thread being run
	 */
	private static Thread clientController(Logger logger,ServerSocket servSock) {
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					try (final Socket clntSock = servSock.accept()){
						clntSock.setSoTimeout(TIMEOUT);
						clntSock.setReuseAddress(true);
						// Wait for a connection
						ClientHandler clntHandler = new ClientHandler(clntSock, logger);
						clntHandler.handleG8RClient(this.getId());
					} catch (IOException ex) {
						logger.log(Level.WARNING, "Client accept failed", ex);
					} catch (ValidationException e) {
						System.err.print(e.getMessage()+"\n");
					}
				}
			}
		};
		thread.start();
		return thread;
	}
}