package G8R.app;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 2
 * Class: CSI 4321
 *
 ************************************************/
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import G8R.serialization.*;

/**
 * This Client takes in user input and command line parameters and 
 * repeatedly sends and receives G8RMessages to and from a server.
 * @version 1.0
 *
 */
public class G8RClient {
	/**
	 * Takes command line arguments and creates a socket, then takes user
	 * input and creates and sends G8RRequests while receiving G8RResponses
	 * from a server.
	 * @param args command line arguments
	 * @throws IOException on I/O problem
	 * @throws ValidationException if invalid token in G8RMessage
	 */
	public static void main(String[] args) throws IOException, 
	ValidationException {
		// Check that user provided correct number of command line arguments
		if(args.length!=3) {
			System.err.print("Usage: G8RClient <server identity> "
					+ "<server port> <cookie file>");
			System.exit(1);
		}
		// Scanner to parse through and store user input
		Scanner userIn = new Scanner(System.in);
		// The name of the server to connect to
		String server= args[0];
		// The port to connect to 
		int servPort = Integer.parseInt(args[1]);
		// Create the socket and get the input/output streams
		try(
				Socket socket = new Socket(server, servPort);
				InputStream in = socket.getInputStream();
				OutputStream out = socket.getOutputStream();){
			// CookieList for this sequence of G8RMessages
			CookieList cookies = createCookies(args[2]);
			// Prompt user for initial function
			System.out.print("Function> ");
			// The request to be sent
			G8RRequest request = null;
			while(request==null) {
				try {
					// Create the original request
					request = new G8RRequest(userIn.nextLine(),new String[0],
							cookies);
				}
				// Handle Validation Exception 
				catch(ValidationException v) {
					System.err.print("Invalid input: " + v.getMessage()+"\n");
					// Reprompt user
					System.out.print("Function> ");
				}
			}
			// MessageInput of socket input stream
			MessageInput servIn = new MessageInput(in);
			// MessageOutput of socket output stream
			MessageOutput servOut = new MessageOutput(out);
			// Loop until server sends "NULL" as function
			clientController(servIn,servOut,request,cookies,userIn,args[2]);
			// Save cookies to cookie file
			storeCookiesAndClose(userIn, request, args[2]);
		} catch (ConnectException e) {
			System.err.print("Invalid Port\n");
		} catch(UnknownHostException e) {
			System.err.print("Invalid Server\n");
		}

	}

	/**
	 * This function takes a status and message from a response
	 * and prints to stdout or stderr as appropriate
	 * @param status the status received from response
	 * @param message the message received from response
	 */
	private static void printMessage(String status, String message) {
		if("OK".equals(status)) {
			System.out.print(message);
		}
		else if("ERROR".equals(status)) {
			System.err.print(message);
		}
	}

	/**
	 * Updates the cookies after a response is received
	 * @param cookieList cookie list to add too
	 * @param response the response received
	 * @throws ValidationException if invalid token
	 */
	private static void updateCookies(CookieList cookieList, CookieList otherList) throws ValidationException {
		for(String key : otherList.getNames()) {
			cookieList.add(key, otherList.getValue(key));
		}
	}

	/**
	 * Stores the cookies and closes files
	 * @param userIn user input scanner
	 * @param request G8RRequest sent by user
	 * @param fileName for the CookieFile 
	 * @throws NullPointerException if null object
	 * @throws IOException if I/O Problem
	 */
	private static void storeCookiesAndClose(Scanner userIn, G8RRequest request,
			String fileName) throws NullPointerException, IOException {
		MessageOutput mOut = new MessageOutput(new 
				FileOutputStream(new File(fileName)));
		request.getCookieList().encode(mOut);
		mOut.close();
		userIn.close();
	}

	/**
	 * Creates the initial cookies
	 * @param filename for the cookie file
	 * @return the cookie list
	 * @throws ValidationException if validation problem
	 * @throws IOException if I/O problem
	 */
	private static CookieList createCookies(String filename) throws ValidationException, IOException {
		CookieList cookies;
		try {
			// Read cookies from file specified in command line
			MessageInput mIn = new MessageInput(new FileInputStream(
					filename));
			cookies = new CookieList(mIn);
			mIn.close();
		}
		// Create Empty CookieList if no file exists
		catch (FileNotFoundException f) {
			cookies= new CookieList();
		}
		return cookies;
	}

	/**
	 * Keeps asking for user input and sending requests until the server returns a null
	 * and the program can exit
	 * @param servIn server input stream
	 * @param servOut server output stream
	 * @param request G8RRequest for this client
	 * @param cookies the cookie list for this client
	 * @param userIn the users input
	 * @param filename the name of the cookiefile
	 * @throws IOException if I/O problem
	 * @throws ValidationException if validation problem
	 */
	private static void clientController(MessageInput servIn, MessageOutput servOut, G8RRequest request,
			CookieList cookies, Scanner userIn, String filename) throws IOException, ValidationException {
		while(!("NULL".equals(request.getFunction()))) {
			// Send the request
			request.encode(servOut);
			G8RResponse response = null;
			// Get the response from server
			try {
				response = (G8RResponse)G8RMessage.decode(servIn);
				updateCookies(cookies,response.getCookieList());
				request.setCookieList(cookies);
			}
			// Close the program if error from server
			catch(ValidationException | NullPointerException | IOException responseErr) {
				System.err.print("Invalid Response from Server: "+
						responseErr.getMessage()+"\n");
				storeCookiesAndClose(userIn, request, filename);
				System.exit(1);
			}

			request.setFunction(response.getFunction());
			// Print message from response
			printMessage(response.getStatus(),
					response.getMessage());
			// If the function is not NULL then get parameters for new 
			// request
			if(!("NULL".equals(request.getFunction()))) {
				boolean invalid = true;
				while(invalid) {
					try {
						String params="";
						// Get user input if there is any
						if(userIn.hasNextLine()) {
							params = userIn.nextLine();		
						}
						// If no user input send empty parameters
						if(params.isEmpty()) {
							request.setParams(new String[0]);
						}
						else {
							request.setParams(params.split(" "));
						}
						invalid=false;
					} 
					catch (ValidationException reqErr) {
						System.err.print("Invalid user input: " + 
								reqErr.getMessage()	+"\n");
						printMessage(response.getStatus(),
								response.getMessage());
					}
				}
			}
		}
	}
}