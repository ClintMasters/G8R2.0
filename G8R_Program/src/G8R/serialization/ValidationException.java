package G8R.serialization;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
import java.io.Serializable;
import java.util.Objects;

/**
 * Validation exception containing the token failing validation
 * @version 1.0
 *
 */
public class ValidationException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;
    private String token;

    /**
     * Constructs validation exception
     * @param msg exception message
     * @param token token causing validation failure
     * @param cause exception cause
     * @throws NullPointerException if parameter is null
     */
    public ValidationException(String msg,
                               String token,
                               Throwable cause)
            throws NullPointerException{
        super(msg, cause);
        this.token= Objects.requireNonNull(token);
    }

    /**
     * Constructs validation exception
     * @param msg exception message
     * @param token token causing validation failure
     * @throws NullPointerException if parameter is null
     */
    public ValidationException(String msg,
                               String token)
            throws NullPointerException {
        this(msg, token, null);
    }

    /**
     * Gets the token for the exception
     * @return the token
     */
    public String getToken(){
        return token;
    }
}
