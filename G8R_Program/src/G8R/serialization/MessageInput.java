/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization;


import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Deserialization input source for messages
 *
 */
public class MessageInput {
    private InputStream mIn;
    /**
     * Constructs a new input source from an InputStream
     * @param in byte input source
     * @throws NullPointerException if in is null
     */
    public MessageInput(InputStream in)
            throws NullPointerException{
        mIn = Objects.requireNonNull(in);
    }

    /**
     * Reads in a byte of input from the input stream
     * @return the byte read
     * @throws IOException on bad input
     */
    public int read() throws IOException {
        return mIn.read();
    }
    
    /**
     * Closes the input stream
     * @throws IOException on I/O problem
     */
    public void close() throws IOException{
    	mIn.close();
    }




}
