/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization;


import java.io.EOFException;
import java.io.IOException;
import java.util.Objects;

/**
 * Represents generic portion of a G8R message and provides
 * serialization/deserialization
 * @version 1.0
 */
public abstract class G8RMessage {
    // represents the header of a G8RRequest
    private static final String requestHead = "G8R/1.0 Q ";
    // Represents the head of a G8RResponse
    private static final String responseHead = "G8R/1.0 R ";
    // Size of G8R Header
    private static final int HEADSIZE = 10;
    // String to store the response function
    private String function;
    // String to store the cookie list
    private CookieList cookies;
    // Encoding for the G8RMessage
    private static final String encoding = "ASCII";

    /**
     * Creates a new G8R message by deserializing form the given input
     * according to the specified serialization
     * @param in user input source
     * @return a new G8R message
     * @throws ValidationException if validation fails
     * @throws IOException if I/O problem
     * @throws NullPointerException if in is null
     */
    public static G8RMessage decode(MessageInput in)
            throws ValidationException, IOException{
        /* If it has Q return a new request
         * if it has R return a new response
         */
        String header = "";
        // Read until the header is read or the end of stream
        while(header.length()<HEADSIZE){
            byte buf=(byte)in.read();

            header += new String(new byte[]{buf}, encoding);
            if(buf==-1){
                throw new EOFException("Hit end of input early");
            }
        }
        if(requestHead.equals(header)){
            return new G8RRequest(in);
        }
        else if(responseHead.equals(header)){
            return new G8RResponse(in);
        }
        else{
            throw new ValidationException("Invalid header: Must be 'G8R/1.0 "
                    + "(Q|R)'",header);
        }
    }

    /**
     * encode the entire G8R message abstracted so that the classes that
     * extend G8RMessage have to implement their own version
     * @param out serialization output sink
     * @throws IOException if I/O problem
     * @throws NullPointerException if out is null
     */
    public abstract void encode(MessageOutput out) throws IOException;
    
    /**
     * return function
     * @return function
     */
    public String getFunction(){
        return function;
    }
    
    /**
     * Set function
     * @param function new function
     * @throws ValidationException if invalid command
     * @throws NullPointerException if null command
     */
    public void setFunction(String function) throws ValidationException{
        this.function = Objects.requireNonNull(function,
                "function cannot be null");
        if(isInvalidToken(function)){
            throw new ValidationException("Invalid function token", function);
        }

    }
    
    /**
     * return cookie list
     * @return cookie list
     */
    public CookieList getCookieList(){
        return new CookieList(cookies);
    }

    /**
     * set cookie list
     * @param cookieList new cookie list
     * @throws NullPointerException if null cookie list
     */
    public void setCookieList(CookieList cookieList){
        this.cookies = new CookieList(Objects.requireNonNull(cookieList,
                "CookieList cannot be null"));
    }

    /**
     * Tests whether or not a token is invalid
     * @param token token to be tested
     * @return whether or not the token is invalid
     */
    public static boolean isInvalidToken(String token) {
        if(token.length()<1){
            return true;
        }
        char[] validToken = token.toCharArray();
        // Return true if any character is not alphanumeric
        for(char ch : validToken){
            if(!(Character.isLetterOrDigit(ch))){
                return true;
            }
        }
        return false;
    }
}
