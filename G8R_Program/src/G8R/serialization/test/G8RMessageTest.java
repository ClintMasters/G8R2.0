/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;


import G8R.serialization.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import static G8R.serialization.G8RMessage.decode;
import static org.junit.jupiter.api.Assertions.*;

class G8RMessageTest {
    /**
     * Test decoding
     * @throws IOException if I/O Problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testDecode() throws IOException, ValidationException {
        byte[] message = "G8R/1.0 Q RUN FCN P1 P2\r\nx=1\r\ny=2\r\n\r\n"
        		.getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(message));
        G8RMessage g8rMsg = decode(in);
        assertTrue(g8rMsg instanceof G8RRequest);
    }

    /**
     * Test decoding Response
     * @throws IOException if I/O Problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testDecodeResponse() throws IOException, ValidationException {
        byte[] message = "G8R/1.0 R OK FCN TestMessage\r\nx=1\r\ny=2\r\n\r\n"
        		.getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(message));
        G8RMessage g8rMsg = decode(in);
        assertTrue(g8rMsg instanceof G8RResponse);
    }

    /**
     * Test throwing IOException
     */
    @Test
    void testDecodeIOException() {
        assertThrows(IOException.class, () -> {
           MessageInput in = new MessageInput(new InputStream() {
               @Override
               public int read() throws IOException {
                   throw new IOException("Testing Decode G8R Message" +
                           " IOException");
               }
           });
           decode(in);
        });
    }

    /**
     * Test throwing null Pointer Exception
     */
    @Test
    void testDecodeNull() {
        assertThrows(NullPointerException.class, () ->{
            decode(null);
        });
    }

    /**
     * Test Validation Exception
     */
    @Test
    void testDecodeValidationException(){
        assertThrows(ValidationException.class, () -> {
            byte[] message = "G8R/1.0 Q R U N F CN P1 P2\r\nx= 1\r\ny=2\r\n\r\n"
            		.getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            decode(in);
        });
    }
    
    /**
     * Test Validation Exception when invalid header
     */
    @Test
    void testDecodeValidationExceptionHeader(){
        assertThrows(ValidationException.class, () -> {
            byte[] message = ("G8R/ 2.0 K RUN FCN P1 P2\r\nx= 1\r\n" +
                    "y=2\r\n\r\n").getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            decode(in);
        });
    }
    
    /**
     * Test throwing EOFException
     */
    @Test
    void testDecodeEOFException() {
        assertThrows(EOFException.class, () -> {
            byte[] message = "G8R/".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            decode(in);
        });
    }
}