package G8R.serialization.test;
/************************************************
 *
 * Author: Clint Masters & Justin Ritter
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
import G8R.serialization.CookieList;
import G8R.serialization.MessageInput;
import G8R.serialization.ValidationException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CookieListTestConstructor {
    private List<String> expNames =
            new ArrayList<>(Arrays.asList("Test", "Cookie"));
    private List<String> expValues =
            new ArrayList<>(Arrays.asList("1", "2"));


    /**********************************************************
     * Tests for the constructor passed another CookieList
     *********************************************************/

    /**
     * Test the easy success route for creating CookieList from another
     * CookieList
     * @throws IOException on an error with input or output
     * @throws ValidationException if the message in messageInput is bad
     */
    @Test
    void testCookieListCookieListSuccess() throws IOException,
            ValidationException {
        String expStr="";
        for(int i = 0; i < expNames.size(); i++) {
            expStr += (expNames.get(i) + "=" + expValues.get(i) + "\r\n");
        }
        expStr += "\r\n";

        ByteArrayInputStream bIn = new ByteArrayInputStream(expStr.getBytes());
        MessageInput mIn = new MessageInput(bIn);
        CookieList testCookie = new CookieList(mIn);
        CookieList cookieCopy = new CookieList(testCookie);
        assertNotNull(cookieCopy);
    }

    /**
     * Tests that a NullPointerException is thrown when
     * the parameter passed in is NULL.
     */
    @Test
    void testCookieListCookieListNullException() {

        assertThrows(NullPointerException.class, () -> {
            CookieList cookie = null;
            new CookieList(cookie);
        });
    }


    /***************************************************
     * Tests of constructor passed a MessageInput
     **************************************************/


    /**
     * Tests the success case for passing a MessageInput as a parameter
     * @throws IOException on an error with input or output
     * @throws ValidationException if the message in messageInput is bad
     */
    @Test
    void testCookieListMessageInput() throws IOException, ValidationException {
        String expStr="";
        for(int i = 0; i < expNames.size(); i++) {
            expStr += (expNames.get(i) + "=" + expValues.get(i) + "\r\n");
        }
        expStr += "\r\n";

        ByteArrayInputStream bIn = new ByteArrayInputStream(expStr.getBytes());
        MessageInput mIn = new MessageInput(bIn);
        new CookieList(mIn);
    }

    /**
     * Tests throwing an IOException using a mock object
     */
    @Test
    void testCookieListMessageInputIOException() {
        assertThrows(IOException.class, () -> {
            MessageInput in = new MessageInput(new InputStream() {
                @Override
                public int read() throws IOException {
                    throw new IOException("Testing that CookieList " +
                            "throws IOException");
                }
            });
            new CookieList(in);
        });
    }

    /**
     * Tests throwing an input validation by passing bad input
     */
    @Test
    void testCookieListMessageInputValidationException() {
        assertThrows(ValidationException.class, () -> {
            String encode = "a =1\r\nb =2\r\n\r\n";
            MessageInput in = new MessageInput(
                    new ByteArrayInputStream(encode.getBytes("ASCII")));
            new CookieList(in);
        });

    }
    
    /**
     * Tests throwing validation exception when cookielist key or value
     * contains '='
     */
    @Test
    void testCookieListMessageInputValidationExceptionEquals() {
        assertThrows(ValidationException.class, () -> {
            String encode = "a=1\r\nb=2=3\r\n\r\n";
            MessageInput in = new MessageInput(
                    new ByteArrayInputStream(encode.getBytes("ASCII")));
            new CookieList(in);
        });

    }

    /**
     * Test valid decode
     * @throws ValidationException if invalid token
     * @throws IOException if I/O problem
     */
    @Test
    void testDecodeValid() throws ValidationException, IOException {
        byte[] twoCookieLists = "x=1\r\ny=2\r\n\r\na=3\r\n\r\n".getBytes(
                "ASCII");
        MessageInput in = new MessageInput(new
                ByteArrayInputStream(twoCookieLists));
        CookieList cookieList = new CookieList(in);
        assertEquals("1", cookieList.getValue("x"));
        assertEquals("2", cookieList.getValue("y"));
        cookieList = new CookieList(in);
        assertEquals("3", cookieList.getValue("a"));
        assertThrows(EOFException.class, () -> new CookieList(in));
    }

    /**
     * Tests throwing a NullPointerException by passing null input
     */
    @Test
    void testCookieListMessageInputNullPointerException() {
        assertThrows(NullPointerException.class, () -> {
            new CookieList((MessageInput)null);
        });
    }

}