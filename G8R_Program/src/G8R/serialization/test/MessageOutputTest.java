/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.MessageOutput;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;

public class MessageOutputTest {
    /**
     * Test valid constructor for MessageOutput
     */
    @Test
    void testMessageOutputConstructorValid(){
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageOutput out = new MessageOutput(bout);
        assertNotNull(out);
    }

    /**
     * Test throwing NullPointerException from constructor
     */
    @Test
    void testMessageOutputNullConstructor(){
        assertThrows(NullPointerException.class, () ->{
           new MessageOutput(null);
        });
    }

    /**
     * Test MessageOutput write validity
     * @throws IOException if I/O Problem
     */
    @Test
    void testMessageOutputWriteValid() throws IOException{
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageOutput out = new MessageOutput(bout);
        out.write("Testing".getBytes("ASCII"));
        assertEquals("Testing",new String(bout.toByteArray(),"ASCII"));
    }

    /**
     * Testing throwing IOException from write method
     */
    @Test
    void testMessageOutputWriteIOException() {
        assertThrows(IOException.class, () -> {
            MessageOutput out = new MessageOutput(new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                    throw new IOException("Testing write IOException");
                }
            });
            out.write("Testing".getBytes("ASCII"));
        });
    }

}