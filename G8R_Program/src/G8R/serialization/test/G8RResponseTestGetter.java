/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class G8RResponseTestGetter {
    private G8RResponse testResponse;
    private CookieList testCookie;
    private String testFunction;
    private String testStatus;
    private String testMessage;

    G8RResponseTestGetter() throws IOException, ValidationException {
        byte[] cookies = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        testFunction = "FCN";
        testCookie = new CookieList(in);
        testMessage = "ThisMessageIsATest";
        testStatus = "OK";
        testResponse = new G8RResponse(testStatus, testFunction, testMessage, 
        		testCookie);
    }

    /**
     * Test whether can change cookie list after getting
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testGetCookieListEncapsulation() throws ValidationException {
        CookieList encapCookie = testResponse.getCookieList();
        encapCookie.add("Create","Failure");
        encapCookie=testResponse.getCookieList();
        assertEquals(testCookie,encapCookie);
    }

    /**
     * Test getting function
     */
    @Test
    void testGetFunction(){
        assertEquals(testFunction,testResponse.getFunction());
    }

    /**
     * Test getting message
     */
    @Test
    void testGetMessage(){
        assertEquals(testMessage,testResponse.getMessage());
    }

    /**
     * Test getting status
     */
    @Test
    void testGetStatus(){
        assertEquals(testStatus, testResponse.getStatus());
    }

    /**
     * Test toString
     */
    @Test
    void testToString() {
        assertEquals("G8RResponse\nStatus: OK\nFunction: FCN\nMessage:"
        		+ " ThisMessageIsATest\nCookies: Cookies=[x=1,y=2]",
        		testResponse.toString());
    }

    /**
     * Test encode
     * @throws IOException if I/O problem
     */
    @Test
    void testEncode() throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageOutput out = new MessageOutput(bout);
        testResponse.encode(out);
        assertEquals("G8R/1.0 R OK FCN ThisMessageIsATest"
        		+ "\r\nx=1\r\ny=2\r\n\r\n",
        		new String(bout.toByteArray(),"ASCII"));
    }
}
