/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class G8RResponseTestConstructor {
    /**
     * Tests a valid response constructor
     * @throws IOException if I/O Problem
     * @throws ValidationException if invalid Token
     */
    @Test
    void testConstructorValid() throws IOException, ValidationException {
        byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(
        		cookieListTest));
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(in);
        String testStatus = "OK";
        String testMessage = "Testing";
        G8RResponse testResponse= new G8RResponse(testStatus, testFunction, 
        		testMessage, testCookie);
        assertEquals(testStatus, testResponse.getStatus());
        assertEquals(testCookie,testResponse.getCookieList());
        assertEquals(testFunction,testResponse.getFunction());
        assertEquals(testMessage,testResponse.getMessage());
    }

    /**
     * Test valid constructor with input stream
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testConstructorValidInputStream() throws IOException, 
    ValidationException {
        byte[] message = "OK FCN TestMessage\r\nx=1\r\ny=2\r\n\r\n"
        		.getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(message));
        byte[] message2 = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput bin = new MessageInput(new ByteArrayInputStream(
        		message2));
        G8RResponse testResponse = new G8RResponse(in);
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(bin);
        String testStatus = "OK";
        String testMessage = "TestMessage";
        assertEquals(testStatus, testResponse.getStatus());
        assertEquals(testCookie,testResponse.getCookieList());
        assertEquals(testFunction,testResponse.getFunction());
        assertEquals(testMessage,testResponse.getMessage());
    }
    
    /**
     * Test valid constructor with input stream
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testConstructorValidInputStreamSpaces() throws IOException, 
    ValidationException {
        byte[] message = "OK FCN  \r\nx=1\r\ny=2\r\n\r\n"
        		.getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(message));
        byte[] message2 = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput bin = new MessageInput(new ByteArrayInputStream(
        		message2));
        G8RResponse testResponse = new G8RResponse(in);
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(bin);
        String testStatus = "OK";
        String testMessage = " ";
        assertEquals(testStatus, testResponse.getStatus());
        assertEquals(testCookie,testResponse.getCookieList());
        assertEquals(testFunction,testResponse.getFunction());
        assertEquals(testMessage,testResponse.getMessage());
    }
    
    /**
     * Test valid constructor with input stream
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testConstructorValidInputStreamEmptyCookie() throws IOException, 
    ValidationException {
        byte[] message = "OK FCN \r\n\r\n\r\n"
        		.getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(message));
        byte[] message2 = "\r\n\r\n".getBytes("ASCII");
        MessageInput bin = new MessageInput(new ByteArrayInputStream(
        		message2));
        G8RResponse testResponse = new G8RResponse(in);
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(bin);
        String testStatus = "OK";
        String testMessage = "";
        assertEquals(testStatus, testResponse.getStatus());
        assertEquals(testCookie,testResponse.getCookieList(),"CookieTest");
        assertEquals(testFunction,testResponse.getFunction());
        assertEquals(testMessage,testResponse.getMessage(),"MSGTEST");
    }

    /**
     * Test throwing validation exception within constructor
     */
    @Test
    void testConstructorInvalidInputStreamValidationException() {
        assertThrows(ValidationException.class, () -> {
            byte[] message = "OKEYDOKEY FCN TestMessage\r\nx=1\r\ny=2\r\n\r\n"
            		.getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            new G8RResponse(in);
        });
    }
    
    /**
     * Test throwing validation exception for incorrect number of parameters
     */
    @Test
    void testConstructorInvalidParameterNumber() {
        assertThrows(ValidationException.class, () -> {
            byte[] message = "OK \r\nx=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            new G8RResponse(in);
        });
    }
    
    /**
     * Test throwing validation exception for invalid function
     */
    @Test
    void testConstructorInvalidFunctionParameter() {
        assertThrows(ValidationException.class, () -> {
            byte[] message = "OK F\nCN TestMessage\r\nx=1\r\ny=2\r\n\r\n"
            		.getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            new G8RResponse(in);
        });
    }
    
    /**
     * Test throwing validation exception for invalid message
     */
    @Test
    void testConstructorInvalidMessageParameter() {
        assertThrows(ValidationException.class, () -> {
            byte[] message = ("OK FCN Test"+(char)127+
            		"Message\r\nx=1\r\ny=2\r\n\r\n").getBytes("ASCII");
            MessageInput in = new MessageInput(new 
            		ByteArrayInputStream(message));
            new G8RResponse(in);
        });
    }

    /**
     * Test throwing IOException within constructor
     */
    @Test
    void testConstructorInvalidInputStreamIOException() {
        assertThrows(IOException.class, () -> {
            MessageInput in = new MessageInput(new InputStream() {
                @Override
                public int read() throws IOException {
                    throw new IOException("Testing response "
                    		+ "constructor IOException");
                }
            });
            new G8RResponse(in);
        });
    }
    
    /**
     * Test hitting end of file early
     */
    @Test
    void testConstructorEOFException() {
        assertThrows(EOFException.class, () -> {
        	byte[] message = " OK FCN TestMessage".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		message));
            new G8RResponse(in);
        });
    }

    /**
     * Test invalid function token
     */
    @Test
    void testConstructorInValidFunction() {
        assertThrows(ValidationException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            String testFunction = "FC N";
            CookieList testCookie = new CookieList(in);
            String testStatus = "OK";
            String testMessage = "Testing";
            new G8RResponse(testStatus, testFunction, testMessage, testCookie);
        });
    }

    /**
     * Test null function parameter
     */
    @Test
    void testConstructorInValidFunctionNull()  {
        assertThrows(NullPointerException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            CookieList testCookie = new CookieList(in);
            String testStatus = "OK";
            String testMessage = "Testing";
            new G8RResponse(testStatus, null, testMessage, testCookie);
        });
    }

    /**
     * Test invalid status parameter
     */
    @Test
    void testConstructorInValidStatus() {
        assertThrows(ValidationException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            String testFunction = "FCN";
            CookieList testCookie = new CookieList(in);
            String testStatus = "TEST";
            String testMessage = "Testing";
            new G8RResponse(testStatus, testFunction, testMessage, testCookie);
        });
    }

    /**
     * Test null status parameter
     */
    @Test
    void testConstructorInValidStatusNull() {
        assertThrows(NullPointerException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            String testFunction = "FCN";
            CookieList testCookie = new CookieList(in);
            String testMessage = "Testing";
            new G8RResponse(null, testFunction, testMessage, testCookie);
        });
    }

    /**
     * Test invalid message parameter
     */
    @Test
    void testConstructorInValidMessage() {
        assertThrows(ValidationException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            String testFunction = "FCN";
            CookieList testCookie = new CookieList(in);
            String testStatus = "OK";
            String testMessage = "Testing"+(char)0+"Blah";
            new G8RResponse(testStatus, testFunction, testMessage, testCookie);
        });
    }

    /**
     * Test null message parameter
     */
    @Test
    void testConstructorInValidMessageNull() {
        assertThrows(NullPointerException.class, () -> {
            byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
            MessageInput in = new MessageInput(new ByteArrayInputStream(
            		cookieListTest));
            String testFunction = "FCN";
            CookieList testCookie = new CookieList(in);
            String testStatus = "OK";
            new G8RResponse(testStatus, testFunction, null, testCookie);
        });
    }

    /**
     * Test null cookieList parameter
     */
    @Test
    void testConstructorInValidCookieListNull() {
        assertThrows(NullPointerException.class, () -> {
            String testFunction = "FCN";
            String testStatus = "OK";
            String testMessage = "Testing";
            new G8RResponse(testStatus, testFunction, testMessage, null);
        });
    }

    /**
     * Tests a valid response constructor
     * @throws IOException if I/O Problem
     * @throws ValidationException if invalid Token
     */
    @Test
    void testEqualsConsistentValid() throws IOException, ValidationException {
        byte[] cookieListTest = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(
        		cookieListTest));
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(in);
        String testStatus = "OK";
        String testMessage = "Testing";
        G8RResponse testResponse= new G8RResponse(testStatus, testFunction, 
        		testMessage, testCookie);
        G8RResponse testResponse2= new G8RResponse(testStatus, testFunction,
        		testMessage, testCookie);
        assertTrue(testResponse.equals(testResponse2));
    }
    
}