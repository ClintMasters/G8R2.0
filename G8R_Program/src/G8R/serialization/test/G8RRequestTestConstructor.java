/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.CookieList;
import G8R.serialization.G8RRequest;
import G8R.serialization.MessageInput;
import G8R.serialization.ValidationException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class G8RRequestTestConstructor {
	/**
	 * Test valid constructor for G8RRequest
	 * 
	 * @throws IOException
	 *             if I/O problem
	 * @throws ValidationException
	 *             if invalid token
	 */
	@Test
	void testConstructorValid() throws IOException, ValidationException {
		byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
		MessageInput in = new MessageInput(new ByteArrayInputStream(
				cookieList));
		String testFunction = "TestingFunction";
		CookieList testCookie = new CookieList(in);
		String[] testParams = { "This", "is", "only", "a", "test" };
		G8RRequest testRequest = new G8RRequest(testFunction, testParams, 
				testCookie);
		assertTrue(Arrays.equals(testParams, testRequest.getParams()));
		assertEquals(testCookie, testRequest.getCookieList());
		assertEquals(testFunction, testRequest.getFunction());
	}

	/**
	 * Test constructor null cookie parameter
	 */
	@Test
	void testConstructorInValidNullCookie() {
		assertThrows(NullPointerException.class, () -> {
			String testFunction = "TestingFunction";
			String[] testParams = { "This", "is", "only", "a", "test" };
			new G8RRequest(testFunction, testParams, null);
		});
	}

	/**
	 * Test constructor invalid function token
	 */
	@Test
	void testConstructorInValidFunction() {
		assertThrows(ValidationException.class, () -> {
			byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					cookieList));
			String testFunction = "Testing Function";
			CookieList testCookie = new CookieList(in);
			String[] testParams = { "This", "is", "only", "a", "test" };
			new G8RRequest(testFunction, testParams, testCookie);
		});

	}

	/**
	 * Test constructor null function parameter
	 */
	@Test
	void testConstructorInValidNullFunction() {
		assertThrows(NullPointerException.class, () -> {
			byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					cookieList));
			CookieList testCookie = new CookieList(in);
			String[] testParams = { "This", "is", "only", "a", "test" };
			new G8RRequest(null, testParams, testCookie);
		});

	}

	/**
	 * Test constructor invalid param token
	 */
	@Test
	void testConstructorInValidParam() {
		assertThrows(ValidationException.class, () -> {
			byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					cookieList));
			String testFunction = "TestingFunction";
			CookieList testCookie = new CookieList(in);
			String[] testParams = { "This", "is", "on ly", "a", "test" };
			new G8RRequest(testFunction, testParams, testCookie);
		});
	}

	/**
	 * Test constructor null array param
	 */
	@Test
	void testConstructorInValidNullParamArray() {
		assertThrows(NullPointerException.class, () -> {
			byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					cookieList));
			String testFunction = "TestingFunction";
			CookieList testCookie = new CookieList(in);
			new G8RRequest(testFunction, null, testCookie);
		});
	}

	/**
	 * Test constructor null array element
	 */
	@Test
	void testConstructorInValidParamElement() {
		assertThrows(NullPointerException.class, () -> {
			byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					cookieList));
			String testFunction = "TestingFunction";
			CookieList testCookie = new CookieList(in);
			String[] testParams = { "This", "is", null, "a", "test" };
			new G8RRequest(testFunction, testParams, testCookie);
		});
	}

	/**
	 * Test hitting end of file early
	 */
	@Test
	void testConstructorEOFException() {
		assertThrows(EOFException.class, () -> {
			byte[] message = " RUN FCN P1 P2".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					message));
			new G8RRequest(in);
		});
	}

	/**
	 * Test validation exception when incorrect number of parameters in the 
	 * input stream passed to the constructor
	 */
	@Test
	void testConstructorIncorrectNumberParameters() {
		assertThrows(ValidationException.class, () -> {
			byte[] message = " RUN\r\nx=1\r\ny=2\r\n\r\n".getBytes("ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					message));
			new G8RRequest(in);
		});
	}

	/**
	 * Test invalid function in input stream being passed
	 */
	@Test
	void testConstructorInvalidInputStreamFunction() {
		assertThrows(ValidationException.class, () -> {
			byte[] message = " RUN F\nCN P1 P2\r\nx=1\r\n\r\n".getBytes(
					"ASCII");
			MessageInput in = new MessageInput(new ByteArrayInputStream(
					message));
			new G8RRequest(in);
		});
	}

	/**
	 * Test creating G8RRequest from input stream
	 * 
	 * @throws ValidationException
	 *             if invalid parameters
	 * @throws IOException
	 *             if problem with I/O
	 */
	@Test
	void testConstructorInputStreamValid() throws IOException, ValidationException {
		byte[] message = " RUN FCN This is only a test\r\nx=1\r\ny=2\r\n\r\n"
				.getBytes("ASCII");
		MessageInput in = new MessageInput(new ByteArrayInputStream(message));
		G8RRequest test = new G8RRequest(in);
		byte[] cookieList = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
		MessageInput bin = new MessageInput(new ByteArrayInputStream(
				cookieList));
		String testFunction = "FCN";
		CookieList testCookie = new CookieList(bin);
		String[] testParams = { "This", "is", "only", "a", "test" };
		assertTrue(Arrays.equals(testParams, test.getParams()));
		assertEquals(testCookie, test.getCookieList());
		assertEquals(testFunction, test.getFunction());
	}

}