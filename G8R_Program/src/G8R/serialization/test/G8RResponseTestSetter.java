/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.CookieList;
import G8R.serialization.G8RResponse;
import G8R.serialization.MessageInput;
import G8R.serialization.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class G8RResponseTestSetter {
    // This G8RResponse is used to test all the methods
	private G8RResponse testResponse;

    G8RResponseTestSetter() throws IOException, ValidationException {
        byte[] cookies = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        testResponse = new G8RResponse("OK", "FCN",
                "ThisMessageIsATest", new CookieList(in));
    }

    /**
     * Test valid cookie
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid cookie
     */
    @Test
    void testSetCookieList() throws IOException, ValidationException {
        byte[] cookies = "test=1\r\ncookie=45\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        CookieList responseCookie = new CookieList(in);
        testResponse.setCookieList(responseCookie);
        assertEquals(responseCookie,testResponse.getCookieList());
    }

    /**
     * Test null cookie
     */
    @Test
    void testSetCookieListNullPointer() {
        assertThrows(NullPointerException.class, () -> {
           testResponse.setCookieList(null);
        });
    }

    /**
     * Test valid function
     * @throws ValidationException if invalid command
     */
    @Test
    void testSetFunction() throws ValidationException {
        testResponse.setFunction("TestingSetter");
        assertEquals("TestingSetter",testResponse.getFunction());
    }

    /**
     * Test invalid function
     * @param testStr function to be tested
     */
    @ParameterizedTest
    @ValueSource(strings = {"Space Test", "new\nLine", "", "pu.nc(tu)at,ion"})
    void testSetFunctionValidationException(String testStr){
        assertThrows(ValidationException.class, () ->{
           testResponse.setFunction(testStr);
        });
    }

    /**
     * Test null function
     */
    @Test
    void testSetFunctionNullPointer(){
        assertThrows(NullPointerException.class, () ->{
           testResponse.setFunction(null);
        });
    }

    /**
     * Test valid message
     * @throws ValidationException if invalid message
     */
    @Test
    void testSetMessage() throws ValidationException {
        testResponse.setMessage("TestingSetMessage");
        assertEquals("TestingSetMessage",testResponse.getMessage());
    }

    /**
     * Test invalid message
     * @param testStr message to be tested
     */
    @ParameterizedTest
    @ValueSource(strings = {("H"+((char)0)+"I"), (char)31+"Test",
    		(char)127+"ok", "error"+(char)10})
    void testSetMessageValidationException(String testStr) {
        assertThrows(ValidationException.class, () -> {
            testResponse.setMessage(testStr);
        });
    }

    /**
     * Test null message
     */
    @Test
    void testSetMessageNullPointer(){
        assertThrows(NullPointerException.class, () -> {
           testResponse.setMessage(null);
        });
    }

    /**
     * Test Valid status
     * @throws ValidationException if status is invalid
     */
    @Test
    void testSetStatus() throws ValidationException {
        testResponse.setStatus("ERROR");
        assertEquals("ERROR",testResponse.getStatus());
    }

    /**
     * Test invalid status
     * @param testStr status to be tested
     */
    @ParameterizedTest
    @ValueSource(strings = {"HI", "Failure", "ok", "error", "Test"})
    void testSetStatusValidationException(String testStr) {
        assertThrows(ValidationException.class, () -> {
            testResponse.setStatus(testStr);
        });
    }

    /**
     * Test null status
     */
    @Test
    void testSetStatusNullPointer(){
        assertThrows(NullPointerException.class, () ->{
            testResponse.setStatus(null);
        });
    }
}
