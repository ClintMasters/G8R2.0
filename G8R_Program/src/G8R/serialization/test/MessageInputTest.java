/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.MessageInput;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class MessageInputTest {
    /**
     * Test valid MessageInput Constructor
     * @throws IOException if I/O problem
     */
    @Test
    void testMessageInputValidConstructor() throws IOException {
        ByteArrayInputStream bin = new ByteArrayInputStream("test".getBytes());
        MessageInput in = new MessageInput(bin);
        assertNotNull(in);
        assertEquals('t',(char)in.read());
    }

    /**
     * Tests MessageInput null parameter
     */
    @Test
    void testMessageInputNullParameter(){
        assertThrows(NullPointerException.class, () ->{
            new MessageInput(null);
        });
    }

    /**
     * Test valid read
     * @throws IOException if I/O problem
     */
    @Test
    void testMessageInputValidRead() throws IOException {
        ByteArrayInputStream bin = new ByteArrayInputStream("test".getBytes());
        MessageInput in = new MessageInput(bin);
        assertEquals('t',(char)in.read());
        assertEquals('e',(char)in.read());
    }

    /**
     * Test throwing IOException from read
     */
    @Test
    void testMessageInputInvalidRead() throws IOException {
        assertThrows(IOException.class, () -> {
            MessageInput in = new MessageInput(new InputStream() {
                @Override
                public int read() throws IOException {
                    throw new IOException("Testing throwing exception " +
                            "from read");
                }
            });
            in.read();
        });
    }


}