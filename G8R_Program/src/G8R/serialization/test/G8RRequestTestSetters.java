/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.CookieList;
import G8R.serialization.G8RRequest;
import G8R.serialization.MessageInput;
import G8R.serialization.ValidationException;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.*;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;


public class G8RRequestTestSetters {
    // This G8RRequest is used to test the methods
	private G8RRequest testRequest;

    G8RRequestTestSetters() throws IOException, ValidationException {
        byte[] cookies = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        String testFunction = "FCN";
        CookieList testCookie = new CookieList(in);
        String[] testParams = {"This", "is", "only", "a", "test"};
        testRequest = new G8RRequest(testFunction, testParams, testCookie);
    }

    /**
     * tests setting the cookie list
     * @throws IOException if I/O problem
     * @throws ValidationException on invalid token
     */
    @Test
    void testSetCookieList() throws IOException, ValidationException {
        byte[] cookies = "x=1\r\ncookie=45\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        CookieList cookie = new CookieList(in);
        testRequest.setCookieList(cookie);
        assertEquals(cookie,testRequest.getCookieList());
    }

    /**
     * Test throwing null pointer from cookie list
     */
    @Test
    void testSetCookieListNullPointer(){
        assertThrows(NullPointerException.class, () ->{
           testRequest.setCookieList(null);
        });
    }

    /**
     * Tests setting function
     * @throws ValidationException if invalid function
     */
    @Test
    void testSetFunction() throws ValidationException {
        testRequest.setFunction("TestingSetter");
        assertEquals("TestingSetter",testRequest.getFunction());
    }

    /**
     * Tests invalid function
     * @param testStr function to be tested
     */
    @ParameterizedTest
    @ValueSource(strings = {"Space Test", "new\nLine", "", "pu.nc(tu)at,ion"})
    void testSetFunctionValidationException(String testStr){
        assertThrows(ValidationException.class, () ->{
            testRequest.setFunction(testStr);
        });
    }

    /**
     * Tests null function
     */
    @Test
    void testSetFunctionNullPointer(){
        assertThrows(NullPointerException.class, () ->{
            testRequest.setFunction(null);
        });
    }

    /**
     * Tests valid parameters
     * @throws ValidationException if parameters are invalid
     */
    @Test
    void testSetParams() throws ValidationException {
        String[] testSetParams = {"Setting", "To", "Something", "NEW"};
        testRequest.setParams(testSetParams);
        assertTrue(Arrays.equals(testSetParams,testRequest.getParams()));
    }

    /**
     * Tests invalid parameters
     */
    @Test
    void testSetParamsValidationExceptionSpaces(){
        assertThrows(ValidationException.class, () ->{
           testRequest.setParams(new String[]{"tes t", "Spa ces"});
        });
    }

    /**
     * Test null parameter array
     */
    @Test
    void testSetParamsNullPointerExceptionArray(){
        assertThrows(NullPointerException.class, () -> {
            testRequest.setParams(null);
        });
    }

    /**
     * test null parameter element
     */
    @Test
    void testSetParamsNullPointerExceptionElement(){
        assertThrows(NullPointerException.class, () ->{
            testRequest.setParams(new String[]{"test", null});
        });
    }
}
