/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class G8RRequestTestGetters {
    private G8RRequest testRequest;
    private CookieList testCookie;
    private String testFunction;
    private String[] testParams = {"This", "is", "only", "a", "test"};

    public G8RRequestTestGetters() throws IOException, ValidationException {
        byte[] cookies = "x=1\r\ny=2\r\n\r\n".getBytes("ASCII");
        MessageInput in = new MessageInput(new ByteArrayInputStream(cookies));
        testFunction = "FCN";
        testCookie = new CookieList(in);
        testRequest = new G8RRequest(testFunction,testParams,testCookie);
    }

    /**
     * Test getting cookies
     */
    @Test
    void testGetCookieList() {
        assertEquals(testCookie,testRequest.getCookieList());
    }

    /**
     * Test Encapsulating cookies
     * @throws IOException if I/O problem
     * @throws ValidationException if invalid token
     */
    @Test
    void testGetCookieListEncapsulation() throws IOException, 
    ValidationException {
        CookieList encapCookie = testRequest.getCookieList();
        encapCookie.add("Create","Failure");
        encapCookie=testRequest.getCookieList();
        assertEquals(testCookie,encapCookie);
    }

    /**
     * Test get function
     */
    @Test
    void testGetFunction(){
        assertEquals(testFunction,testRequest.getFunction());
    }

    /**
     * Test get params
     */
    @Test
    void testGetParams(){
        assertTrue(Arrays.equals(testParams,testRequest.getParams()));
    }

    /**
     * Test changing params after getting
     */
    @Test
    void testGetParamsEncapsulation(){
        String[] testCap = testRequest.getParams();
        testCap[0] = "nothing";
        assertTrue(Arrays.equals(testParams,testRequest.getParams()));
    }

    /**
     * Test to string
     */
    @Test
    void testToString() {
        assertEquals("G8RRequest\nCommand: RUN\nFunction: FCN\nParam: "
        		+ "[This,is,only,a,test]\nCookies=[x=1,y=2]",
        		testRequest.toString());
    }

    /**
     * Test encode
     * @throws IOException if I/O problem
     * @throws ValidationException 
     */
    @Test
    void testEncode() throws IOException, ValidationException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageOutput out = new MessageOutput(bout);
        testRequest.encode(out);
        assertEquals("G8R/1.0 Q RUN FCN This is only a "
        		+ "test\r\nx=1\r\ny=2\r\n\r\n",
        		new String(bout.toByteArray(),"ASCII"));
    }
}
