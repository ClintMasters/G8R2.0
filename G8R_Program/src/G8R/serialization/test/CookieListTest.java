package G8R.serialization.test;
/************************************************
 *
 * Author: Clint Masters & Justin Ritter
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import G8R.serialization.CookieList;
import G8R.serialization.MessageInput;
import G8R.serialization.MessageOutput;
import G8R.serialization.ValidationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class CookieListTest {
    // The test cookie list
    private CookieList testCookie;
    // The expected cookieList string
    private String expStr="";
    // Encoding for the G8RMessage
    private static final String encoding = "ASCII";

    CookieListTest() throws IOException, ValidationException {
        List<String> expNames = new ArrayList<>(Arrays.asList("Cookie", "Elderberry", "Father", "Hamster",
                "Mother", "Test"));
        for(int i = 0; i < expNames.size(); i++) {
            List<String> expValues = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5", "6"));
            expStr += (expNames.get(i) + "=" + expValues.get(i) + "\r\n");
        }
        expStr += "\r\n";
        ByteArrayInputStream bytesIn =
                new ByteArrayInputStream(expStr.getBytes(encoding));
        MessageInput in = new MessageInput(bytesIn);
        testCookie = new CookieList(in);
    }
    @DisplayName("Encode Tests")
    @Nested
    class EncodeTests {
        /*********************************************
         * Tests of the encode method.
         ********************************************/

        /**
         * Test the success case for encoding a CookieList
         *
         * @throws IOException on error with input or output
         */
        @Test
        void testEncode() throws IOException {
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            MessageOutput out = new MessageOutput(byteOut);
            testCookie.encode(out);
            assertEquals(expStr, new String(byteOut.toByteArray(),encoding));
        }

        /**
         * Test throwing an IOException using mock object
         */
        @Test
        void testEncodeIOException()  {
            assertThrows(IOException.class, () -> {
                MessageOutput out = new MessageOutput(new OutputStream() {
                    @Override
                    public void write(int b) throws IOException {
                        throw new IOException("Test IOException");
                    }
                });
                testCookie.encode(out);
            });
        }

        /**
         * Test the success case for encoding a CookieList
         */
        @Test
        void testEncodeNullPointerException() {
            assertThrows(NullPointerException.class, () -> {
                MessageOutput out = null;
                CookieList encodeCookie = new CookieList();
                encodeCookie.encode(out);
            });
        }
    }

    @DisplayName("Add Tests")
    @Nested
    class AddTests {
        /**************************************
         * Tests for the add method
         ************************************/

        /**
         * Test adding values successfully to CookieList
         *
         * @throws ValidationException if validation failure for name or value
         */
        @Test
        void testAdd() throws ValidationException {
            int initSize = testCookie.size();
            testCookie.add("Hello", "4");
            assertEquals(initSize + 1, testCookie.size());
        }

        /**
         * Test adding duplicate keys to the map successfully
         *
         * @throws ValidationException if validation failure for name or value
         */
        @Test
        void testAddDuplicateKey() throws ValidationException {
            int initSize = testCookie.size();
            testCookie.add("Name3", "4");
            testCookie.add("Name3", "5");
            assertEquals(initSize + 1, testCookie.size());
            assertEquals("5", testCookie.getValue("Name3"));
        }

        /**
         * Test throwing ValidationException by passing bad name
         */
        @Test
        void testAddValidationExceptionBadName() {
            assertThrows(ValidationException.class, () -> {
                testCookie.add("a-c", "4");
            });

        }

        /**
         * Test throwing ValidationException by passing bad value
         */
        @Test
        void testAddValidationExceptionBadValue() {
            assertThrows(ValidationException.class, () -> {
                testCookie.add("Name3", "Val\r\n3");
            });

        }

        /**
         * Test throwing NullPointer by passing null key
         */
        @Test
        void testAddNullPointerExceptionNullKey() {
            assertThrows(NullPointerException.class, () -> {
                testCookie.add(null, "4");
            });
        }

        /**
         * Test throwing NullPointer by passing bad value
         */
        @Test
        void testAddNullPointerExceptionNullValue() {
            assertThrows(NullPointerException.class, () -> {
                testCookie.add("Name3", null);
            });

        }
    }

    /*****************************************
     * Tests for the toString method
     ****************************************/

    /**
     * Tests toString success case where the CookieList is empty
     */
    @Test
    void testToStringEmpty() {
        CookieList stringCookie = new CookieList();
        assertEquals("Cookies=[]", stringCookie.toString());
    }

    /**
     * Success case when the CookieList is not empty
     */
    @Test
    void testToStringNonEmpty() {
        assertEquals("Cookies=[Cookie=1,Elderberry=2,Father=3,Hamster=4," +
                        "Mother=5,Test=6]", testCookie.toString()
                , "Failure on non-empty List");
    }


    /*********************************************
     * Tests for the getNames method
     *******************************************/

    /**
     * Tests the success case for getting names from a CookieList
     */
    @Test
    void testGetNames() {
        Set<String> names = testCookie.getNames();
        assertTrue(names.contains("Test"),"Did not get Test");
        assertTrue(names.contains("Cookie"),"Did not get Cookie");
        assertTrue(names.size()==6,"Did not return correct number of names");

    }

    /**
     * Success case when the CookieList is empty
     */
    @Test
    void testGetNamesEmpty() {
        CookieList nameCookie = new CookieList();
        Set<String> names = nameCookie.getNames();
        assertTrue(names.isEmpty(),"Failure on empty CookieList");
    }


    /********************************************
     * Tests for the getValue method
     *******************************************/

    /**
     * Success case for getting a value from a CookieList
     */
    @Test
    void testGetValue() {
        String value = testCookie.getValue("Test");
        assertEquals("6", value);
    }

    /**
     * Success case for when the CookieList is empty
     */
    @Test
    void testGetValueEmpty() {
        CookieList valueCookie = new CookieList();
        assertNull(valueCookie.getValue("test"));
    }

    /************************************************
     * Tests for the hashCode method
     ***********************************************/

    /**
     * Tests that the hashCode function works on the same object
     */
    @Test
    void testHashCodeSameObject() {
        assertEquals(testCookie.hashCode(),testCookie.hashCode(),
                "Did not return same hashcode for same object");
    }

    /**
     * Test that the hashCode function works on equal objects
     */
    @Test
    void testHashCodeEqualObject() {
        CookieList hashCookie = new CookieList(testCookie);

        assertEquals(testCookie.hashCode(),hashCookie.hashCode(),
                "Did not return same hashcode for equal objects");
    }

    @DisplayName("Equals Tests")
    @Nested
    class EqualsTests {
        /*********************************************
         * Tests for the Equals method
         ********************************************/

        /**
         * Tests the equal method for when the objects have the same CookieList
         */
        @Test
        void testEqualsObjectEqual() {
            CookieList equalCookie1 = new CookieList(testCookie);
            assertTrue(testCookie.equals(equalCookie1));
        }

        /**
         * Tests that the equals method works when the objects are unequal
         */
        @Test
        void testEqualsObjectUnEqual() {
            CookieList equalCookie1 = new CookieList();
            assertFalse(testCookie.equals(equalCookie1));
        }

        /**
         * Tests the reflexive property of the equals method which is
         * that x.equals(x) should return true
         */
        @Test
        void testEqualsObjectEqualReflexive() {
            assertTrue(testCookie.equals(testCookie));
        }

        /**
         * Tests the symmetric property of equals which is that if x.equals(y)
         * is true then y.equals(x) should be true
         */
        @Test
        void testEqualsObjectEqualSymmetric() {
            CookieList equalCookie1 = new CookieList(testCookie);
            assertTrue(equalCookie1.equals(testCookie));
            assertTrue(testCookie.equals(equalCookie1));
        }

        /**
         * Tests the transitive property of equals which is that if x.equals(y)
         * and y.equals(z) then x.equals(z)
         */
        @Test
        void testEqualsObjectEqualTransitive() {
            CookieList equalCookie1 = new CookieList(testCookie);
            CookieList equalCookie2 = new CookieList(testCookie);
            assertTrue(equalCookie1.equals(equalCookie2));
            assertTrue(equalCookie2.equals(testCookie));
            assertTrue(equalCookie1.equals(testCookie));
        }

        /**
         * Tests that equals is consistent when the objects are unequal
         */
        @Test
        void testEqualsObjectUnEqualConsistent() {
            CookieList equalCookie1 = new CookieList();
            assertFalse(equalCookie1.equals(testCookie));
            assertFalse(equalCookie1.equals(testCookie));
        }

        /**
         * Tests that equals is consistent when the objects are equal
         */
        @Test
        void testEqualsObjectEqualConsistent() {
            CookieList equalCookie1 = new CookieList(testCookie);
            assertEquals(equalCookie1.equals(testCookie),
                    equalCookie1.equals(testCookie));

        }

        /**
         * Test that x.equals(null) returns false
         */
        @Test
        void testEqualsObjectEqualNull() {
            assertFalse(testCookie.equals(null));
        }
    }
}
