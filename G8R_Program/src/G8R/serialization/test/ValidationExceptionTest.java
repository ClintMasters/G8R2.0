/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization.test;

import G8R.serialization.ValidationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidationExceptionTest {
    /**
     * Test Validation exception constructor and get token
     */
    @Test
    void testValidationExceptionConstructorAndGetTokenValid(){
        ValidationException test = new ValidationException("TestMSG",
                "Token",new Throwable("TestingValidationException"));
        assertNotNull(test);
        assertEquals("Token",test.getToken());
    }

    /**
     * Test null token in constructor
     */
    @Test
    void testValidationExceptionNullToken(){
        assertThrows(NullPointerException.class, () ->{
            new ValidationException("Msg", null);
        });
    }

}