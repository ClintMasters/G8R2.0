/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.Objects;

/**
 * Represents a G8R response and provides serialization/deserialization
 * @version 1.0
 */
public class G8RResponse extends G8RMessage {
	// String to store the response status
	private String status;
	// String to store the response message
	private String message;
	// String to store the response Delimiter
	private static final String DELIM = "\r\n";
	// Encoding for the G8RMessage
	private static final String encoding = "ASCII";
	// Response header
	private static final String HEADER = "G8R/1.0 R ";

	/**
	 * Constructs G8R response using given values
	 * @param status response status
	 * @param function response function
	 * @param message response message
	 * @param cookies response cookie list
	 * @throws ValidationException if error with given values
	 * @throws NullPointerException if null parameter
	 */
	public G8RResponse(String status, String function, String message,
			CookieList cookies) throws ValidationException {
		// Assign the status
		this.setStatus(Objects.requireNonNull(status));

		// Assign the function
		this.setFunction(Objects.requireNonNull(function,
				"Function cannot be null"));
		// Assign the message
		this.setMessage(Objects.requireNonNull(message));

		// Assign the cookie list
		this.setCookieList(new CookieList(cookies));    
	}
	
	/**
	 * Default constructor necessary for server
	 */
	public G8RResponse() {
		
	}

	/**
	 * Create new G8R Response from messageInput
	 * @param in input source
	 * @throws IOException if I/O Problem
	 * @throws ValidationException if invalid value
	 * @throws NullPointerException if in is null
	 */
	public G8RResponse(MessageInput in) throws IOException,
	ValidationException {
		Objects.requireNonNull(in, "in can't be null");
		String store = "";
		// Read until stream is empty or the delimiter is found
		while(!(store.contains(DELIM))){
			byte buf=(byte)in.read();
			if(buf==-1){
				throw new EOFException("Hit end of input early");
			}
			store += new String(new byte[]{buf}, encoding);
		}
		store = store.replace(DELIM,"");
		String[] responseArray = store.split(" ");
		if(responseArray.length<2){
			throw new ValidationException("Must be at least 2 arguments "
					+ "for a Response",store);
		}
		// Set the status
		this.setStatus(Objects.requireNonNull(responseArray[0],
				"Status cannot be null"));
		// Set the function
		this.setFunction(Objects.requireNonNull(responseArray[1],
				"Function cannot be null"));
		String storeMsg = "";
		if(responseArray.length!=2) {
			storeMsg = Objects.requireNonNull(responseArray[2],
					"Message cannot be null");
			for(int i = 3;i<responseArray.length;i++) {
				storeMsg+=" "+Objects.requireNonNull(responseArray[i],
						"Message cannot contain null");
			}
			if(store.endsWith(" ")) {
				storeMsg+=" ";
			}
		}
		else if(store.endsWith("  ")) {
			storeMsg=" ";
		}
		// Set the message
		this.setMessage(storeMsg);
		// Set the cookies
		this.setCookieList(new CookieList(in));
	}

	/**
	 * Returns string representation of G8R response message
	 * @return string representation
	 */
	@Override
	public String toString(){
		return ("G8RResponse\nStatus: "+status+"\nFunction: "+
				this.getFunction()+"\nMessage: "+message+"\nCookies: " +
				this.getCookieList());
	}

	/**
	 * encode the entire G8R Response
	 * @param out serialization output sink
	 * @throws IOException if I/O problem
	 * @throws NullPointerException if out is null
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {
		Objects.requireNonNull(out,"MessageOutput cannot be null");
		out.write((HEADER+status+" "+this.getFunction()+" "+message+
				DELIM).getBytes(encoding));
		this.getCookieList().encode(out);
	}

	/**
	 * return status
	 * @return status
	 */
	public String getStatus(){
		return status;
	}

	/**
	 * set status
	 * @param status new status
	 * @throws ValidationException if invalid status
	 * @throws NullPointerException if null status
	 */
	public void setStatus(String status) throws ValidationException{
		if(isInvalidStatus(status)){
			throw new ValidationException("Status must be 'OK' or 'Error'",
					status);
		}
		this.status = Objects.requireNonNull(status, "Status cannot be null");
	}

	/**
	 * return message
	 * @return message
	 */
	public String getMessage(){
		return message;
	}

	/**
	 * set message
	 * @param message new message
	 * @throws ValidationException if invalid message
	 * @throws NullPointerException if null message
	 */
	public void setMessage(String message) throws ValidationException{
		if(isInvalidMessage(message)) {
			throw new ValidationException("Message must contain " +
					"only printable characters", message);
		}
		this.message = Objects.requireNonNull(message,
				"Message cannot be null");
	}

	/**
	 * Tests whether a message is invalid
	 * @param message message to test
	 * @return true if invalid
	 */
	private boolean isInvalidMessage(String message){
		char[] checkMsg = message.toCharArray();
		// Check that every character is printable
		for(char ch : checkMsg){
			if((int)ch<32||(int)ch>126){
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns whether or not a status is invalid
	 * @param status status to test
	 * @return true if invalid
	 */
	private boolean isInvalidStatus(String status){
		// If status is not OK or ERROR return true
		if(!("OK".equals(status))&&!("ERROR".equals(status))){
			return true;
		}
		return false;
	}

	/**
	 * Implement according to Object.hashCode()
	 */
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	/**
	 * Implement according to Object.equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if same object
		if(this==obj) {
			equality = true;
		}
		// Tests if same type of object
		else if(!(obj instanceof G8RResponse)) {
			equality = false;
		}
		// Tests if have same parts
		else {
			G8RResponse other = (G8RResponse)obj;
			if(this.getCookieList().equals(other.getCookieList())
					&& this.getFunction().equals(other.getFunction())
					&& this.getMessage().equals(other.getMessage())
					&& this.getStatus().equals(other.getStatus())) {
				equality = true;
			}
		}
		return equality;
	}
}
