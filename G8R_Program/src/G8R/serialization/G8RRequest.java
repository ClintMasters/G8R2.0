/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 1
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a G8R request and provides serialization/deserialization
 * @version 1.0
 */
public class G8RRequest extends G8RMessage {
	// Array to store the parameters for the request
	private String[] params;
	// Constant to represent the delimiter in a request
	private static final String DELIM = "\r\n";
	// Encoding for the G8RMessage
	private static final String encoding = "ASCII";
	// G8RRequest header
	private static final String HEADER = "G8R/1.0 Q RUN ";

	/**
	 * Creates a new G8R request using given values
	 * @param function request function
	 * @param params request parameters
	 * @param cookies request cookie list
	 * @throws ValidationException if error with given values
	 * @throws NullPointerException if null parameter
	 */
	public G8RRequest(String function, String[] params, CookieList cookies)
			throws ValidationException {
		// Assign the function
		this.setFunction(Objects.requireNonNull(function,
				"function cannot be null"));
		// Assign the parameters
		this.setParams(Objects.requireNonNull(params,
				"Parameter Array cannot be null"));
		// Assign the cookies
		this.setCookieList(new CookieList(cookies));
	}

	/**
	 * Creates new G8R request using MessageInput
	 * @param in input source
	 * @throws IOException if I/O problem
	 * @throws ValidationException if invalid command
	 * @throws NullPointerException if null parameter
	 */
	public G8RRequest(MessageInput in) throws IOException,
	ValidationException {
		Objects.requireNonNull(in,"in can't be null");
		String store = "";
		// Read until stream is empty or delimiter is hit
		while(!(store.contains(DELIM))){
			byte buf=(byte)in.read();

			if(buf==-1){
				throw new EOFException("Hit end of input early");
			}
			store += new String(new byte[]{buf}, encoding);
		}
		// Split up string into various parts of request
		store = store.replace(DELIM,"");
		String [] requestArray = store.split(" ");


		if(!("RUN".equals(store.substring(0, store.indexOf(" "))))) {
			throw new ValidationException("Invalid Request", store);
		}
		store = store.substring(store.indexOf(" ")+1, store.length());
		if(requestArray.length<2){
			throw new ValidationException("Must be at least " +
					"1 argument for a Request",store);
		}
		// Assign function
		this.setFunction(Objects.requireNonNull((store.contains(" ") ? store.substring(0,store.indexOf(" ")):
			store)));
		if(store.contains(" ")) {
			store = store.substring(store.indexOf(" ")+1,store.length());
			// Assign parameters
			String[] storeArr = store.split(" ");
			this.setParams(storeArr.clone());
		}
		// Assign cookies
		this.setCookieList(new CookieList(in));
	}

	/**
	 * Returns string representation of G8R request message
	 * @return string representation
	 */
	@Override
	public String toString() {
		String paramStr = "[";
		// Write out each parameter to a string
		if(params!=null) {
			for(String str : params){
				if(!(str.equals(params[params.length-1]))) {
					paramStr += str + ",";
				}
				else{
					paramStr+=str;
				}
			}
		}
		paramStr+="]";
		// Combine the parts of request into human readable components
		return ("G8RRequest\nCommand: RUN\nFunction: "+this.getFunction()
		+"\nParam: "+paramStr+"\n" + this.getCookieList());
	}

	/**
	 * encode the entire G8R Request
	 * @param out serialization output sink
	 * @throws IOException if I/O problem
	 * @throws NullPointerException if out is null
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {
		Objects.requireNonNull(out,"MessageOutput cannot be null");
		// Write out the header and function for request
		out.write((HEADER+this.getFunction()).getBytes(encoding));
		// Write out the parameters
		for(int i = 0; i< params.length;i++){
			out.write((" "+params[i]).getBytes(encoding));

		}
		out.write(DELIM.getBytes(encoding));
		// Encode the cookies
		this.getCookieList().encode(out);
	}

	/**
	 * return parameter list
	 * @return parameter list
	 */
	public String[] getParams() {
		return params.clone();
	}

	/**
	 * set parameters
	 * @param params new parameters
	 * @throws ValidationException if invalid params
	 * @throws NullPointerException if null array or array element
	 */
	public void setParams(String[] params) throws ValidationException {
		for(String checkParam : params){
			Objects.requireNonNull(checkParam,
					"Cannot have null array element");
			if(isInvalidToken(checkParam)){
				throw new ValidationException("Invalid parameter token",
						checkParam);
			}
		}
		this.params = Objects.requireNonNull(params.clone(),
				"Parameter Array cannot be null");
	}

	/**
	 * Implement according to Object.hashCode()
	 */
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	/**
	 * Implement according to Object.equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if same object
		if(this==obj) {
			equality = true;
		}
		// Tests if same type of object
		else if(!(obj instanceof G8RRequest)) {
			equality = false;
		}
		// Tests if have same parts
		else {
			G8RRequest other = (G8RRequest)obj;
			if(this.getCookieList().equals(other.getCookieList())
					&& this.getFunction().equals(other.getFunction())
					&& Arrays.equals(this.getParams(), other.getParams())) {
				equality = true;
			}
		}
		return equality;
	}
}
