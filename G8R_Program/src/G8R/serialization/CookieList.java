package G8R.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.*;

import static G8R.serialization.G8RMessage.isInvalidToken;
/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/

/**
 * List of cookies (name/value pairs)
 * @version 1.0
 */
public class CookieList {
    // This represents the delimeter for cookie list
    private static final String DELIM = "\r\n";
    //  Represents the ending of a cookielist
    private static final String ENDING = "\r\n\r\n";
    // The map that actually stores cookie keys and values
    private Map<String, String> cookieMap = new TreeMap<>();
 // Encoding for the G8RMessage
    private static final String encoding = "ASCII";

    /**
     * Creates a new, empty cookie list
     * Empty constructor because we need parameterless
     */
    public CookieList() { }

    /**
     * Creates a new, cloned cookie list
     * @param cookieList list of cookies to clone
     * @throws NullPointerException if cookieList is null
     */
    public CookieList(CookieList cookieList) {
        Objects.requireNonNull(cookieList,"cookieList cannot be null");
        cookieMap.putAll(cookieList.cookieMap);
    }

    /**
     * Creates a new CookieList by decoding the input stream
     * @param in input stream from which to deserialize the name/value list
     * @throws ValidationException if validation problem such as
     * illegal name and/or value, etc
     * @throws IOException if I/O problem (EOFException for EOS)
     * @throws NullPointerException if input stream is null
     */
    public CookieList(MessageInput in)
            throws ValidationException,
            IOException {
        Objects.requireNonNull(in,"input cannot be null");
        String store = "";
        // Read until end of stream or end of cookie list
        while(!(store.contains(ENDING)) && !(DELIM.equals(store))) {
            byte buf=(byte)in.read();

            store += new String(new byte[]{buf}, encoding);
            if(buf==-1){
                throw new EOFException("Hit end of input early");
            }
        }
        store.replace(ENDING,"");
        // Split up the string into each cookie
        String[] cookieArr = store.split(DELIM);
        for(String str : cookieArr){
            // Split each cookie into key and value
            String[] keyValue = str.split("=");
            if(keyValue.length!=2){
                throw new ValidationException("Did not encode the cookie" +
                        " properly, key or value cannot contain '='", str);
            }
            else if(isInvalidToken(keyValue[0])){
                throw new ValidationException("Invalid token on key",
                        keyValue[0]);
            }
            else if(isInvalidToken(keyValue[1])){
                throw new ValidationException("Invalid token on value",
                        keyValue[1]);
            }
            // Add the key value pair
            cookieMap.put(keyValue[0],keyValue[1]);
        }
    }

    /**
     * Encode the name-value list.
     * @param out serialization output sink
     * @throws IOException if I/O problem
     * @throws NullPointerException if out is null
     */
    public void encode(MessageOutput out)
            throws IOException {
        Objects.requireNonNull(out,"MessageOutput cannot be null");

        for(String key : cookieMap.keySet()) {
            out.write(key.getBytes(encoding));
            out.write("=".getBytes(encoding));
            out.write(cookieMap.get(key).getBytes(encoding));
            out.write(DELIM.getBytes(encoding));
        }
        out.write(DELIM.getBytes(encoding));
        out.flush();
    }

    /**
     * Adds the name/value pair. If the name already exists, the new
     * value replaces the old value.
     * @param name name to be added
     * @param value value to be associated with the name
     * @throws ValidationException if validation failure for name or value
     * @throws NullPointerException if name or value is null
     */
    public void add(String name, String value)
            throws ValidationException {
        if(isInvalidToken(name)){
            throw new ValidationException("Invalid name token", name);
        }
        if(isInvalidToken(value)){
            throw new ValidationException("Invalid value token", value);
        }
        cookieMap.put(Objects.requireNonNull(name),
                Objects.requireNonNull(value));
    }

    /**
     * Returns string representation of cookie list. The name-value pair
     * serialization must be in sort order (alphabetically by name
     * in increasing order).
     * @return string representation of cookie list
     */
    @Override
    public String toString(){
        String retString = "Cookies=[";
        int cnt = 1;
        // Add each key value pair to the string to return
        for(String key : cookieMap.keySet()){
            retString += key + "=" + cookieMap.get(key);
            if(cnt<cookieMap.size()){
                retString += ",";
                cnt++;
            }
        }
        retString+="]";
        return retString;
    }

    /**
     * Gets the set of names
     * @return Set (potentially empty) of names (strings) for this list
     */
    public Set<String> getNames() {
        return new HashSet<>(cookieMap.keySet());
    }

    /**
     * Gets the value associated with the given name
     * @param name cookie name
     * @return Value associated with the given name or null if no such name
     */
    public String getValue(String name){
        return cookieMap.get(name);
    }

    /**
     * Implement according to Object.hashCode()
     */
    @Override
    public int hashCode() {
        return cookieMap.hashCode();
    }

    /**
     * Implement according to Object.equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equality;
        // Tests if same object
        if(this==obj) {
            equality = true;
        }
        // Tests if same type of object
        else if(!(obj instanceof CookieList)) {
            equality = false;
        }
        // Tests if have same key value pairs
        else {
            CookieList list2 = (CookieList)obj;
            equality = cookieMap.equals(list2.cookieMap);
        }
        return equality;
    }

    /**
     * Returns the amount of cookies in the CookieList
     * @return integer representing the number of cookies in the CookieList
     */
    public int size(){
        return cookieMap.size();
    }
    
    /**
     * Returns whether or not the cookie list has no cookies.
     * @return boolean true if the list is empty false otherwise
     */
    public boolean isEmpty() {
    	return cookieMap.isEmpty();
    }

}
