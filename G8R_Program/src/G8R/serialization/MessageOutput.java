/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 0
 * Class: CSI 4321
 *
 ************************************************/
package G8R.serialization;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

/**
 * Serialization output source for messages
 *
 */
public class MessageOutput {
    private OutputStream messageOut;
    /**
     * Constructs a new output source from an OutputStream
     * @param out byte output sink
     * @throws NullPointerException if out is null
     */
    public MessageOutput(OutputStream out)
            throws NullPointerException {
        messageOut = Objects.requireNonNull(out);
    }

    /**
     * Writes a byte array to the output stream
     * @param buf the byte array to be written
     * @throws IOException on bad output
     */
    public void write(byte[] buf) throws IOException {
        messageOut.write(buf);
    }

    /**
     * flushes the output stream
     * @throws IOException on bad output
     */
    public void flush() throws IOException{
        messageOut.flush();
    }

    /**
     * closes the output stream
     * @throws IOException if I/O problem
     */
    public void close() throws IOException{
        messageOut.close();
    }
}
