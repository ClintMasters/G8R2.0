Clint Masters Presents: RPS (Rock, Paper, Scissors)
The added application for this server is the RPS application. 
RPS stands for the classic game: Rock, Paper, Scissors
To play this game when prompted for the initial function the user should enter
"RPS" which will send a request with a function value of RPS. The server
will then ask the user to choose, either "Rock", "Paper", or "Scissors". After
the user makes their choice the server will respond with whether the user
won, lost, or tied and how many times total they have won, lost, or tied based
on a cookie list being passed to and from the server.