/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

import java.util.Objects;

/**
 * Represents an N4M query and provides serialization/derserialization
 * @version 1.0
 *
 */
public class N4MQuery extends N4MMessage {
	// Header for the query
	private static final byte HEADER = 0b00100000;
	// Position of the length in the frame
	private static final byte LENGTHPOS = 2;
	// Where the data starts in the query frame
	private static final int DATAOFFSET = 3;
	// Maximum byte size for 
	private static final int MAXBYTESIZE = 255;
	// Business Name for the query
	private String businessName;

	/**
	 * Creates a new N4M query using given values
	 * @param msgId message ID
	 * @param businessName business name
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if null parameter
	 */
	public N4MQuery(int msgId, String businessName) throws N4MException {
		setMsgId(msgId);
		setBusinessName(businessName);
		setErrorCodeNum(0);
	}

	/**
	 * Creates a new N4MQuery from a byte array
	 * @param in contains input bytes
	 * @param msgId message ID
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if null parameter
	 */
	protected N4MQuery(byte[] in, int msgId) throws N4MException {
		int nameLength=in[0];
		String name="";
		try {
			for(int i = 0;i<nameLength;i++) {
				name+=(char)in[i+1];
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new N4MException("Incorrect Business Name Length",ErrorCodeType.BADMSGSIZE);
		}
		setBusinessName(name);
		setMsgId(msgId);
		setErrorCodeNum(0);
	}

	/**
	 * Encodes a N4M query to a byte array
	 * @return byte array of N4MQuery
	 */
	@Override
	public byte[] encode() {
		byte header = (byte) (HEADER);
		byte[] outArr = new byte[businessName.length()+3];
		outArr[0]=header;
		outArr[1]=(byte)getMsgId();
		outArr[LENGTHPOS]=(byte)businessName.length();
		for(int i = 0;i<businessName.length();i++) {
			outArr[i+DATAOFFSET]=(byte)businessName.charAt(i);
		}
		return outArr;
	}

	/**
	 * Return human-readable representation
	 * @return human-readable string
	 */
	@Override
	public String toString() {
		return super.toString()+"\nBusiness Name: "+getBusinessName();
	}

	/**
	 * Return business name
	 * @return business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Set business name
	 * @param businessName business name
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if business name is null
	 */
	public void setBusinessName(String businessName) throws N4MException {
		this.businessName=Objects.requireNonNull(businessName, "Business Name cannot be null");
		if(businessName.length()>MAXBYTESIZE) {
			throw new N4MException("Business Name is too long", ErrorCodeType.BADMSGSIZE);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setErrorCodeNum(int errorCodeNum) throws N4MException {
		if(errorCodeNum!=0) {
			throw new N4MException("Query ErrorCodeNum must be 0",ErrorCodeType.INCORRECTHEADER);
		}
		super.setErrorCodeNum(errorCodeNum);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getBusinessName(),super.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if same object
		if(this==obj) {
			equality = true;
		}
		// Tests if same type of object
		else if(!(obj instanceof N4MQuery)) {
			equality = false;
		}
		// Tests if have same parts
		else {
			N4MQuery other = (N4MQuery)obj;
			if(super.equals(other)
					&& getBusinessName().equals(other.getBusinessName())){
				equality = true;
			}
		}
		return equality;
	}
}
