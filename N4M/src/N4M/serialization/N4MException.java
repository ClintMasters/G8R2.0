/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

import java.io.Serializable;
import java.util.Objects;

/**
 * N4M validation exception
 * @version 1.0
 *
 */
public class N4MException extends Exception implements Serializable{
	// UID for the serialVersion
	private static final long serialVersionUID = 1L;
	// The ErrorCodeType for this exception
	private ErrorCodeType errorCodeType;
	/**
	 * Constructs N4M validation exception
	 * @param msg exception message
	 * @param errorCodeType type of error
	 * @throws NullPointerException if msg or errorCodeType is null
	 */
	public N4MException(String msg,ErrorCodeType errorCodeType) {
		this(msg, errorCodeType, null);
	}
	
	/**
	 * Constructs N4M validation exception
	 * @param msg exception message
	 * @param errorCodeType type of error
	 * @param cause exception cause
	 * @throws NullPointerException if msg or errorCodeType is null
	 */
	public N4MException(String msg,
            ErrorCodeType errorCodeType,
            Throwable cause) {
		super(msg, cause);
        this.errorCodeType= Objects.requireNonNull(errorCodeType);
	}
	
	/**
	 * Return error code type
	 * @return error code type
	 */
	public ErrorCodeType getErrorCodeType() {
		return errorCodeType;
	}
}
