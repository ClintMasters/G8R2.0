/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents generic portion of a N4M message and provides serialization/deserialization.
 * @version 1.0
 */
public abstract class N4MMessage {
	// the version for message header
	private static final byte version = 0b0010;
	// Mask to determine error code number
	private static final byte ERRORMASK = 0b00000111;
	// Mask to determine query or response
	private static final byte QRMASK = 0b00001000;
	// Maximum number for an unsigned byte
	private static final int MAXBYTESIZE = 255;
	// Byte offset for where data starts in a message
	private static final int DATASTART=2;
	// message ID of the N4MMessage
	private int msgId;
	// Error code number associated with this message
	private int errorCodeNum;
	/**
	 * Creates a new N4M message by deserializing from the given byte array according to the specified serialization.
	 * @param in bufer of received packet
	 * @return new N4M message
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if in is null
	 */
	public static N4MMessage decode(byte[] in) throws N4MException{
		Objects.requireNonNull(in,"Byte Array in cannot be null");
		if((in[0]>>>4)==version) {
			if((in[0]&ERRORMASK)<=4) {
				int msgId = Byte.toUnsignedInt(in[1]);
				if((in[0]&QRMASK)!=0) {
					return new N4MResponse(Arrays.copyOfRange(in, DATASTART, in.length),msgId,
							Byte.toUnsignedInt((byte)(in[0]&ERRORMASK)));
				}
				return new N4MQuery(Arrays.copyOfRange(in, DATASTART, in.length),msgId);
			}
			throw new N4MException("Invalid Error Code", ErrorCodeType.INCORRECTHEADER);
		}
		throw new N4MException("Incorrect Version", ErrorCodeType.INCORRECTHEADER);
	}

	/**
	 * Return encoded N4M message
	 * @return message encoded in byte array
	 */
	public abstract byte[] encode();

	/**
	 * Return human-readable representation
	 * @return human-readable string
	 */
	@Override
	public String toString() {
		return "ErrorCodeNum: "+getErrorCodeNum()+" MsgId: "+getMsgId();
	}

	/**
	 * Set the message ID
	 * @param msgId new message ID
	 * @throws N4MException if validation fails
	 */
	public void setMsgId(int msgId) throws N4MException {
		if(msgId>MAXBYTESIZE||msgId<0) {
			throw new N4MException("Invalid MsgId value",ErrorCodeType.INCORRECTHEADER);
		}
		this.msgId=msgId;
	}

	/**
	 * Return message ID
	 * @return message ID
	 */
	public int getMsgId() {
		return msgId;
	}

	/**
	 * Return error code number
	 * @return error code number
	 */
	public int getErrorCodeNum() {
		return errorCodeNum;
	}

	/**
	 * Set error code by number
	 * @param errorCodeNum new error code number
	 * @throws N4MException if validation fails
	 */
	public void setErrorCodeNum(int errorCodeNum) throws N4MException {
		if(ErrorCodeType.valueOf(errorCodeNum)instanceof ErrorCodeType) {
			this.errorCodeNum=errorCodeNum;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(getErrorCodeNum(),getMsgId());
	}

	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if have same parts

		N4MMessage other = (N4MMessage)obj;
		if(getMsgId()==(other.getMsgId())
				&& getErrorCodeNum()==(other.getErrorCodeNum())){
			equality = true;
		}

		return equality;
	}
}
