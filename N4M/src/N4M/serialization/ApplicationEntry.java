/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

import java.util.Objects;

/**
 * Represents one application and its access count
 * @version 1.0
 *
 */
public class ApplicationEntry {
	// Maximum size 2 byte integer
	private static final int MAXCOUNTSIZE= 65535;
	// Maximum size of byte unsigned
	private static final int MAXBYTESIZE = 255;
	// The number of times an application has been accessed
	private int accessCount;
	// The name of the application
	private String appName;
	
	/**
	 * Create application entry
	 * @param applicationName name of application
	 * @param accessCt application access count
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if applicationName is null
	 */
	public ApplicationEntry(String applicationName,int accessCt)
     throws N4MException {
		setApplicationName(applicationName);
		setAccessCount(accessCt);
	}
	
	/**
	 * Creates new application entry from an existing one
	 * @param app ApplicationEntry to be copied
	 */
	public ApplicationEntry(ApplicationEntry app) {
		this.appName=app.appName;
		this.accessCount=app.accessCount;
	}
	
	/**
	 * Return human-readable representation
	 * @return human-readable string
	 */
	@Override
	public String toString() {
		return "Name: "+getApplicationName()+"\tTimes Accessed: "+getAccessCount();
	}
	
	/**
	 * Return application access count
	 * @return access count
	 */
	public int getAccessCount() {
		return accessCount;
	}
	
	/**
	 * Set application access count
	 * @param accessCount access count
	 * @throws N4MException if validation fails
	 */
	public void setAccessCount(int accessCount) throws N4MException {
		if(accessCount>MAXCOUNTSIZE) {
			throw new N4MException("Access count cannot be greater than 2 bytes unsigned",
					ErrorCodeType.BADMSGSIZE);
		}
		this.accessCount=accessCount;
	}
	
	/**
	 * Returns application name
	 * @return application name
	 */
	public String getApplicationName() {
		return appName;
	}
	
	/**
	 * Set application name
	 * @param applicationName application name
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if applicationName is null
	 */
	public void setApplicationName(String applicationName)
	throws N4MException {
		appName=Objects.requireNonNull(applicationName);
		if(applicationName.length()>MAXBYTESIZE) {
			throw new N4MException("Invalid application name length",
					ErrorCodeType.BADMSGSIZE);
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getApplicationName(),getAccessCount());
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if same object
		if(this==obj) {
			equality = true;
		}
		// Tests if same type of object
		else if(!(obj instanceof ApplicationEntry)) {
			equality = false;
		}
		// Tests if have same parts
		else {
			ApplicationEntry other = (ApplicationEntry)obj;
			if(getApplicationName().equals((other.getApplicationName()))
					&& getAccessCount()==(other.getAccessCount())) {
				equality = true;
			}
		}
		return equality;
	}
}
