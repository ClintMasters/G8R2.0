/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import N4M.serialization.*;

/**
 * Test N4MResponse
 * @author Clint Masters
 *
 */
class N4MResponseTest {
	// Response header
	private static final byte RHEADNOERROR = 0b00101000;
	// Message ID
	private static final int MSGID = 0;
	// Error number
	private static final int ERRORNUM=0;
	// Test list of applications
	private List<ApplicationEntry> testApp = new ArrayList<>();
	// Date used during testing
	private Date testDate = new Date((System.currentTimeMillis()/1000)*1000);
	// Response used during testing
	private N4MResponse testResponse;
	/**
	 * Constructor to set up the tests
	 * @throws N4MException if validation fails
	 */
	N4MResponseTest() throws N4MException {
		testApp.add(new ApplicationEntry("testApp",1));
		testResponse = new N4MResponse(ERRORNUM,MSGID,testDate,testApp);
	}

	/**
	 * Tests if the constructor works
	 * @throws N4MException if validation fails
	 */
	@Test
	void testConstructorValid() throws N4MException {
		int testErrorNum=0;
		int testMsgId=0;
		N4MResponse testResp = new N4MResponse(testErrorNum,testMsgId,
				testDate,testApp);
		assertEquals(testErrorNum,testResp.getErrorCodeNum());
		assertEquals(testMsgId,testResp.getMsgId());
		assertEquals(testApp,testResp.getApplications());
		assertEquals(testDate,testResp.getTimestamp());
	}
	
	/**
	 * tests invalid application count
	 */
	@Test
	void testInvalidApplicationCount() {
		assertThrows(N4MException.class, () ->{
			byte[] header = {RHEADNOERROR,MSGID};
			long sec = testDate.getTime()/1000;
			byte[] timestamp = {(byte)(sec>>>24),(byte)(sec>>>16),(byte)(sec>>>8),(byte)sec};
			byte[] appArr = {2,0,1,4,'t','e','s','t'};
			byte[] in = new byte[appArr.length+header.length+timestamp.length];
			System.arraycopy(header, 0, in, 0, header.length);
			System.arraycopy(timestamp, 0, in, header.length, timestamp.length);
			System.arraycopy(appArr, 0, in, timestamp.length+header.length, appArr.length);
			N4MMessage.decode(in);
		});
	}
	
	/**
	 * Test invalid application name length
	 */
	@Test
	void testInvalidApplicationNameLength() {
		assertThrows(N4MException.class, () ->{
			byte[] header = {RHEADNOERROR,MSGID};
			long sec = testDate.getTime()/1000;
			byte[] timestamp = {(byte)(sec>>>24),(byte)(sec>>>16),(byte)(sec>>>8),(byte)sec};
			byte[] appArr = {1,0,1,5,'t','e','s','t'};
			byte[] in = new byte[appArr.length+header.length+timestamp.length];
			System.arraycopy(header, 0, in, 0, header.length);
			System.arraycopy(timestamp, 0, in, header.length, timestamp.length);
			System.arraycopy(appArr, 0, in, timestamp.length+header.length, appArr.length);
			N4MMessage.decode(in);
		});
	}
	
	/**
	 * Tests null timestamp
	 */
	@Test
	void testNullTimestamp() {
		assertThrows(NullPointerException.class,()->{
			new N4MResponse(0,0,null,testApp);
		});
	}

	/**
	 * Tests a null application list
	 */
	@Test
	void testNullApplications() {
		assertThrows(NullPointerException.class,()->{
			new N4MResponse(0,0,testDate,null);
		});
	}

	/**
	 * Tests getting a timestamp
	 */
	@Test
	void testGetTimestamp() {
		assertEquals(testDate,testResponse.getTimestamp());
	}

	/**
	 * Tests getting a timestamp and keeping encapsulation
	 * @throws N4MException if validation fails
	 */
	@Test
	void testGetTimestampEncapsulation() throws N4MException {
		Date testCap = testResponse.getTimestamp();
		testCap.setTime(1000000000L);
		assertEquals(testDate,testResponse.getTimestamp());
		assertNotEquals(testCap, testResponse.getTimestamp());
	}

	/**
	 * Tests getting list of applications
	 */
	@Test
	void testGetApps() {
		assertEquals(testApp,testResponse.getApplications());
	}

	/**
	 * Tests getting applications while keeping encapsulation
	 * @throws N4MException if validation fails
	 */
	@Test
	void testGetAppsEncapsulation() throws N4MException {
		List<ApplicationEntry> testCap = testResponse.getApplications();
		testCap.get(0).setAccessCount(24);
		assertNotEquals(testCap,testResponse.getApplications());
	}

	/**
	 * Test setting the timestamp
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetTimestamp() throws N4MException {
		testResponse.setTimestamp(new Date(1000L));
		assertEquals(new Date(1000L),testResponse.getTimestamp());
	}

	/**
	 * Test setting invalid timestamp
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetTimestampInvalid() throws N4MException {
		assertThrows(N4MException.class, () -> {
			List<ApplicationEntry> testTime = new ArrayList<>();
			testTime.add(new ApplicationEntry("TEST",0));
			testResponse.setApplications(testTime);
			testResponse.setTimestamp(new Date(1000L));
		});
	}


	/**
	 * Test setting timestamp and keeping encapsulation
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetTimestampEncapsulation() throws N4MException {
		testResponse.setTimestamp(new Date(1000L));
		assertNotEquals(testDate,testResponse.getTimestamp());
	}

	/**
	 * Test invalid timestamp
	 * @param testNum invalid timestamp being tested
	 */
	@ParameterizedTest
	@ValueSource(longs = {-6, -1})
	void testSetTimestampInvalid(long testNum){
		assertThrows(N4MException.class, () ->{
			testResponse.setTimestamp(new Date(testNum));
		});
	}

	/**
	 * Test setting applications
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetApps() throws N4MException {
		List<ApplicationEntry> testCap = new ArrayList<>();
		testCap.add(new ApplicationEntry("encapsulation",2));
		testResponse.setApplications(testCap);
		assertEquals(testCap,testResponse.getApplications());
	}
	
	/**
	 * Tests setting invalid application
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetAppsInvalidAmount() throws N4MException {
		assertThrows(N4MException.class, ()->{
			List<ApplicationEntry> testCap = new ArrayList<>();
			for(int i = 0;i<268;i++) {
				testCap.add(new ApplicationEntry("encapsulation",2));
			}
			testResponse.setApplications(testCap);
		});
	}

	/**
	 * Tests setting applications with encapsulation
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetAppsEncapsulation() throws N4MException {
		List<ApplicationEntry> testCap = new ArrayList<>();
		testCap.add(new ApplicationEntry("encapsulation",2));
		testResponse.setApplications(testCap);
		assertNotEquals(testApp,testResponse.getApplications());
	}

	/**
	 * Tests getting the message ID
	 */
	@Test
	void testGetMsgId() {
		assertEquals(MSGID,testResponse.getMsgId());
	}

	/**
	 * Tests getting teh errorCodeNumber
	 */
	@Test
	void testGetErrorCodeNum()  {
		assertEquals(ERRORNUM,testResponse.getErrorCodeNum());
	}

	/**
	 * Tests response toString
	 */
	@Test
	void testToString() {
		String retStr = "";
		retStr+="ErrorCodeNum: "+ERRORNUM+" MsgId: "+MSGID;
		retStr+="\nTimestamp: "+testDate;
		retStr+="\nApplications:\n";
		for(ApplicationEntry a : testApp) {
			retStr+=a;
		}
		assertEquals(retStr,testResponse.toString());
	}

	/**
	 * Tests response encode
	 * @throws N4MException if validation fails
	 */
	@Test
	void testEncode() throws N4MException {
		byte[] out = testResponse.encode();
		N4MResponse checkEncode = (N4MResponse)N4MMessage.decode(out);
		assertEquals(checkEncode,testResponse);
	}

	/************************************************
	 * Tests for the hashCode method
	 ***********************************************/

	/**
	 * Tests that the hashCode function works on the same object
	 */
	@Test
	void testHashCodeSameObject() {
		assertEquals(testResponse.hashCode(),testResponse.hashCode(),
				"Did not return same hashcode for same object");
	}

	/**
	 * Test that the hashCode function works on equal objects
	 * @throws N4MException 
	 */
	@Test
	void testHashCodeEqualObject() throws N4MException {
		N4MResponse hashResponse = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
		assertEquals(testResponse.hashCode(),hashResponse.hashCode(),
				"Did not return same hashcode for equal objects");
	}

	@DisplayName("Equals Tests")
	@Nested
	class EqualsTests {
		/*********************************************
		 * Tests for the Equals method
		 ********************************************/

		/**
		 * Tests the equal method for when the objects have the same N4MResponse
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqual() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
			assertTrue(testResponse.equals(equalResponse1));
		}

		/**
		 * Tests that the equals method works when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqual() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(1,7,new Date(0),new ArrayList<ApplicationEntry>());
			assertFalse(testResponse.equals(equalResponse1));
		}

		/**
		 * Tests the reflexive property of the equals method which is
		 * that x.equals(x) should return true
		 */
		@Test
		void testEqualsObjectEqualReflexive() {
			assertTrue(testResponse.equals(testResponse));
		}

		/**
		 * Tests the symmetric property of equals which is that if x.equals(y)
		 * is true then y.equals(x) should be true
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualSymmetric() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
			assertTrue(equalResponse1.equals(testResponse));
			assertTrue(testResponse.equals(equalResponse1));
		}

		/**
		 * Tests the transitive property of equals which is that if x.equals(y)
		 * and y.equals(z) then x.equals(z)
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualTransitive() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
			N4MResponse equalResponse2 = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
			assertTrue(equalResponse1.equals(equalResponse2));
			assertTrue(equalResponse2.equals(testResponse));
			assertTrue(equalResponse1.equals(testResponse));
		}

		/**
		 * Tests that equals is consistent when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqualConsistent() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(1,7,new Date(0),new ArrayList<ApplicationEntry>());
			assertFalse(equalResponse1.equals(testResponse));
			assertFalse(equalResponse1.equals(testResponse));
		}

		/**
		 * Tests that equals is consistent when the objects are equal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualConsistent() throws N4MException {
			N4MResponse equalResponse1 = new N4MResponse(ERRORNUM, MSGID,testDate, testApp);
			assertEquals(equalResponse1.equals(testResponse),
					equalResponse1.equals(testResponse));

		}

		/**
		 * Test that x.equals(null) returns false
		 */
		@Test
		void testEqualsObjectEqualNull() {
			assertFalse(testResponse.equals(null));
		}
	}

}
