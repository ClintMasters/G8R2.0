/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import N4M.serialization.ErrorCodeType;
import N4M.serialization.N4MException;

/**
 * Tests for the ErrorCodeTypeTest
 * @author Clint Masters
 *
 */
class ErrorCodeTypeTest {
	/**
	 * Tests getting the valueOf with NoError
	 * @throws N4MException if validation fails
	 */
	@Test
	void testValueOfNoError() throws N4MException {
		assertEquals(ErrorCodeType.NOERROR, ErrorCodeType.valueOf(0));
		assertEquals(ErrorCodeType.NOERROR, ErrorCodeType.valueOf("NOERROR"));

	}
	
	/**
	 * Tests getting the valueOf with IncorrectHeader
	 * @throws N4MException if validation fails
	 */
	@Test
	void testValueOfIncorrectHeader() throws N4MException {
		assertEquals(ErrorCodeType.INCORRECTHEADER, ErrorCodeType.valueOf(1));
		assertEquals(ErrorCodeType.INCORRECTHEADER, ErrorCodeType.valueOf("INCORRECTHEADER"));

	}
	
	/**
	 * Tests getting the valueOf with BadMessageSize
	 * @throws N4MException if validation fails
	 */
	@Test
	void testValueOfMessageSize() throws N4MException {
		assertEquals(ErrorCodeType.BADMSGSIZE, ErrorCodeType.valueOf(2));
		assertEquals(ErrorCodeType.BADMSGSIZE, ErrorCodeType.valueOf("BADMSGSIZE"));
	}
	
	/**
	 * Tests getting the valueOf with BadMessage
	 * @throws N4MException if validation fails
	 */
	@Test
	void testValueOfMessage() throws N4MException {
		assertEquals(ErrorCodeType.BADMSG, ErrorCodeType.valueOf(3));
		assertEquals(ErrorCodeType.BADMSG, ErrorCodeType.valueOf("BADMSG"));
	}
	
	/**
	 * Tests getting the valueOf with SystemError
	 * @throws N4MException if validation fails
	 */
	@Test
	void testValueOfSystemError() throws N4MException {
		assertEquals(ErrorCodeType.SYSTEMERROR, ErrorCodeType.valueOf(4));
		assertEquals(ErrorCodeType.SYSTEMERROR, ErrorCodeType.valueOf("SYSTEMERROR"));
	}
	
	/**
	 * Tests getting valueOf with invalid number
	 */
	@Test
	void testValueOfInvalid() {
		assertThrows(N4MException.class, () ->{
			ErrorCodeType.valueOf(8);
		});
	}
}
