/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import N4M.serialization.N4MException;
import N4M.serialization.N4MMessage;
import N4M.serialization.N4MQuery;

/**
 * Test N4MQuery class
 * @author Clint Masters
 *
 */
class N4MQueryTest {
	// Business name
	private static final String BNAME = "testName";
	// Message ID
	private static final int MSGID = 0;
	// Query header
	private static final byte QHEADNOERROR = 0b00100000;
	// query used for testing
	private N4MQuery testQuery;
	
	/**
	 * Create new query for testing
	 * @throws N4MException if validation fails
	 */
	N4MQueryTest() throws N4MException{
		testQuery=new N4MQuery(MSGID,BNAME);
	}
	
	/**
	 * Tests the constructor
	 * @throws N4MException if validation fails
	 */
	@Test
	void testConstructorValid() throws N4MException {
		testQuery = new N4MQuery(MSGID,BNAME);
		assertEquals(MSGID, testQuery.getMsgId());
		assertEquals(BNAME, testQuery.getBusinessName());
	}
	
	/**
	 * Tests for a bad business name
	 * @throws N4MException if validation fails
	 */
	@Test
	void testInvalidBusinessName() throws N4MException {
		assertThrows(N4MException.class, ()->{
			byte[] header = {QHEADNOERROR,MSGID};
			byte[] name = {5,'t','e','s','t'};
			byte[] in = new byte[name.length+header.length];
			System.arraycopy(header, 0, in, 0, header.length);
			System.arraycopy(name, 0, in, header.length, name.length);
			N4MMessage.decode(in);
		});
	}
	
	/**
	 * Tests if the encode works
	 * @throws N4MException if validation fails
	 */
	@Test
	void testEncodeValid() throws N4MException {
		byte[] out = testQuery.encode();
		N4MQuery checkEncode = (N4MQuery)N4MMessage.decode(out);
		assertEquals(testQuery,checkEncode);
	}
	
	/**
	 * Tests the toString method
	 */
	@Test
	void testToString() {
		String testStr = "ErrorCodeNum: "+0+" MsgId: "+MSGID+
				"\nBusiness Name: "+BNAME;
		assertEquals(testStr, testQuery.toString());
	}
	
	/**
	 * Test getting the business name
	 */
	@Test
	void testGetBusinessName() {
		assertEquals(BNAME, testQuery.getBusinessName());
	}
	
	/**
	 * Tests setting the business name
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetBusinessName() throws N4MException {
		testQuery.setBusinessName("NEWBNAME");
		assertEquals("NEWBNAME", testQuery.getBusinessName());
	}
	
	/**
	 * Tests setting a bad business name
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetBusinessNameInvalid() throws N4MException {
		assertThrows(N4MException.class, () ->{
			String str = "";
			for(int i = 0;i<280;i++) {
				str+=i+"";
			}
			testQuery.setBusinessName(str);	
		});
	}
	
	/**
	 * Tests setting teh error code number
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetErrorCodeNum() throws N4MException {
		testQuery.setErrorCodeNum(0);
		assertEquals(0,testQuery.getErrorCodeNum());
	}
	
	/**
     * Test invalid errorCodeNum
     * @param testNum invalid errorcodenum being tested
     */
    @ParameterizedTest
    @ValueSource(ints = {-6, 289, -1, 1000})
    void testSetErrorCodeNumInvalid(int testNum){
        assertThrows(N4MException.class, () ->{
           testQuery.setErrorCodeNum(testNum);
        });
    }
	/************************************************
     * Tests for the hashCode method
     ***********************************************/

    /**
     * Tests that the hashCode function works on the same object
     */
    @Test
    void testHashCodeSameObject() {
        assertEquals(testQuery.hashCode(),testQuery.hashCode(),
                "Did not return same hashcode for same object");
    }

    /**
     * Test that the hashCode function works on equal objects
     * @throws N4MException 
     */
    @Test
    void testHashCodeEqualObject() throws N4MException {
        N4MQuery hashQuery = new N4MQuery(MSGID,BNAME);
        assertEquals(testQuery.hashCode(),hashQuery.hashCode(),
                "Did not return same hashcode for equal objects");
    }
	
	@DisplayName("Equals Tests")
	@Nested
	class EqualsTests {
		/*********************************************
		 * Tests for the Equals method
		 ********************************************/

		/**
		 * Tests the equal method for when the objects have the same N4MQuery
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqual() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(MSGID,BNAME);
			assertTrue(testQuery.equals(equalQuery1));
		}

		/**
		 * Tests that the equals method works when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqual() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(7,"BADNAME");
			assertFalse(testQuery.equals(equalQuery1));
		}
		
		/**
		 * Tests that the equals method works when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqualNames() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(MSGID,"BADNAME");
			assertFalse(testQuery.equals(equalQuery1));
		}

		/**
		 * Tests the reflexive property of the equals method which is
		 * that x.equals(x) should return true
		 */
		@Test
		void testEqualsObjectEqualReflexive() {
			assertTrue(testQuery.equals(testQuery));
		}

		/**
		 * Tests the symmetric property of equals which is that if x.equals(y)
		 * is true then y.equals(x) should be true
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualSymmetric() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(MSGID,BNAME);
			assertTrue(equalQuery1.equals(testQuery));
			assertTrue(testQuery.equals(equalQuery1));
		}

		/**
		 * Tests the transitive property of equals which is that if x.equals(y)
		 * and y.equals(z) then x.equals(z)
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualTransitive() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(MSGID,BNAME);
			N4MQuery equalQuery2 = new N4MQuery(MSGID,BNAME);
			assertTrue(equalQuery1.equals(equalQuery2));
			assertTrue(equalQuery2.equals(testQuery));
			assertTrue(equalQuery1.equals(testQuery));
		}

		/**
		 * Tests that equals is consistent when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqualConsistent() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(7,"UnequalName");
			assertFalse(equalQuery1.equals(testQuery));
			assertFalse(equalQuery1.equals(testQuery));
		}

		/**
		 * Tests that equals is consistent when the objects are equal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualConsistent() throws N4MException {
			N4MQuery equalQuery1 = new N4MQuery(MSGID,BNAME);
			assertEquals(equalQuery1.equals(testQuery),
					equalQuery1.equals(testQuery));

		}

		/**
		 * Test that x.equals(null) returns false
		 */
		@Test
		void testEqualsObjectEqualNull() {
			assertFalse(testQuery.equals(null));
		}
	}
}
