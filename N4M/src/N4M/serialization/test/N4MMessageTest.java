/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import N4M.serialization.*;

/**
 * Tests for the N4MMessage class
 * @author Clint Masters
 *
 */
class N4MMessageTest {
	// Response header
	private static final byte RHEADNOERROR = 0b00101000;
	// Query header
	private static final byte QHEADNOERROR = 0b00100000;
	// Message ID
	private static final byte MSGID = 0;
	// Current Date
	private Date testDate = new Date((System.currentTimeMillis()/1000)*1000);
	// Message to use in testing
	private N4MMessage testMessage;
	// message not equal to testMessage
	private N4MMessage unequalMessage;
	// Message equal to testMessage
	private N4MMessage equalMessage;
	// Message equal to testMessage
	private N4MMessage equalMessage2;
	
	/**
	 * Constructor to test N4MMessage
	 * @throws N4MException if validation fails
	 */
	N4MMessageTest() throws N4MException{
		byte[] header = {QHEADNOERROR,MSGID};
		byte[] name = {4,'t','e','s','t'};
		byte[] badname = {5,'f','a','l','s','e'};
		byte[] in = new byte[name.length+header.length];
		byte[] badIn = new byte[badname.length+header.length];
		System.arraycopy(header, 0, in, 0, header.length);
		System.arraycopy(name, 0, in, header.length, name.length);
		System.arraycopy(header, 0, badIn, 0, header.length);
		System.arraycopy(badname, 0, badIn, header.length, badname.length);
		testMessage = N4MMessage.decode(in);
		unequalMessage = N4MMessage.decode(badIn);
		equalMessage = N4MMessage.decode(in);
		equalMessage2 = N4MMessage.decode(in);
	}
	
	/**
	 * Test whether decode works for response
	 * @throws N4MException if validation fails
	 */
	@Test
	void testDecodeValidResponse() throws N4MException {
		byte[] header = {RHEADNOERROR,MSGID};
		long sec = testDate.getTime()/1000;
		byte[] timestamp = {(byte)(sec>>>24),(byte)(sec>>>16),(byte)(sec>>>8),(byte)sec};
		byte[] appArr = {1,0,1,4,'t','e','s','t'};
		byte[] in = new byte[appArr.length+header.length+timestamp.length];
		System.arraycopy(header, 0, in, 0, header.length);
		System.arraycopy(timestamp, 0, in, header.length, timestamp.length);
		System.arraycopy(appArr, 0, in, timestamp.length+header.length, appArr.length);
		N4MMessage testMsg = N4MMessage.decode(in);
		assertTrue(testMsg instanceof N4MResponse);
	}
	
	/**
	 * Tests whether decode works for query
	 * @throws N4MException if validation fails
	 */
	@Test
	void testDecodeValidQuery() throws N4MException {
		byte[] header = {QHEADNOERROR,MSGID};
		byte[] name = {4,'t','e','s','t'};
		byte[] in = new byte[name.length+header.length];
		System.arraycopy(header, 0, in, 0, header.length);
		System.arraycopy(name, 0, in, header.length, name.length);
		N4MMessage testMsg = N4MMessage.decode(in);
		assertTrue(testMsg instanceof N4MQuery);
	}
	
	/**
	 * Tests invalid version
	 * @throws N4MException if validation fails
	 */
	@Test
	void testDecodeInvalidVersion() throws N4MException {
		assertThrows(N4MException.class, ()->{
			byte badhead = 0b01000000;
			byte[] header = {badhead,MSGID};
			byte[] name = {4,'t','e','s','t'};
			byte[] in = new byte[name.length+header.length];
			System.arraycopy(header, 0, in, 0, header.length);
			System.arraycopy(name, 0, in, header.length, name.length);
			N4MMessage.decode(in);	
		});
	}
	
	/**
	 * Tests invalid error
	 * @throws N4MException if validation fails
	 */
	@Test
	void testDecodeInvalidError() throws N4MException {
		assertThrows(N4MException.class, ()->{
			byte badhead = 0b00100111;
			byte[] header = {badhead,MSGID};
			byte[] name = {4,'t','e','s','t'};
			byte[] in = new byte[name.length+header.length];
			System.arraycopy(header, 0, in, 0, header.length);
			System.arraycopy(name, 0, in, header.length, name.length);
			N4MMessage.decode(in);	
		});
	}
	
	/**
     * Test invalid msgId
     * @param testId invalid Id being tested
     */
    @ParameterizedTest
    @ValueSource(ints = {-6, 289, -1, 1000})
    void testSetMsgIdInvalid(int testId){
        assertThrows(N4MException.class, () ->{
           testMessage.setMsgId(testId);
        });
    }
    
    /**
     * Test invalid errorCodeNum
     * @param testNum invalid errorcodenum being tested
     */
    @ParameterizedTest
    @ValueSource(ints = {-6, 289, -1, 1000})
    void testSetErrorCodeNumInvalid(int testNum){
        assertThrows(N4MException.class, () ->{
           testMessage.setErrorCodeNum(testNum);
        });
    }
    
	/************************************************
     * Tests for the hashCode method
     ***********************************************/

    /**
     * Tests that the hashCode function works on the same object
     */
    @Test
    void testHashCodeSameObject() {
        assertEquals(testMessage.hashCode(),testMessage.hashCode(),
                "Did not return same hashcode for same object");
    }

    /**
     * Test that the hashCode function works on equal objects
     * @throws N4MException 
     */
    @Test
    void testHashCodeEqualObject() throws N4MException {
        assertEquals(testMessage.hashCode(),equalMessage.hashCode(),
                "Did not return same hashcode for equal objects");
    }
	
	@DisplayName("Equals Tests")
	@Nested
	class EqualsTests {
		/*********************************************
		 * Tests for the Equals method
		 ********************************************/

		/**
		 * Tests the equal method for when the objects have the same N4MMessage
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqual() throws N4MException {
			assertTrue(testMessage.equals(equalMessage));
		}

		/**
		 * Tests that the equals method works when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqual() throws N4MException {
			assertFalse(testMessage.equals(unequalMessage));
		}

		/**
		 * Tests the reflexive property of the equals method which is
		 * that x.equals(x) should return true
		 */
		@Test
		void testEqualsObjectEqualReflexive() {
			assertTrue(testMessage.equals(testMessage));
		}

		/**
		 * Tests the symmetric property of equals which is that if x.equals(y)
		 * is true then y.equals(x) should be true
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualSymmetric() throws N4MException {
			assertTrue(equalMessage.equals(testMessage));
			assertTrue(testMessage.equals(equalMessage));
		}

		/**
		 * Tests the transitive property of equals which is that if x.equals(y)
		 * and y.equals(z) then x.equals(z)
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualTransitive() throws N4MException {
			assertTrue(equalMessage.equals(equalMessage2));
			assertTrue(equalMessage2.equals(testMessage));
			assertTrue(equalMessage.equals(testMessage));
		}

		/**
		 * Tests that equals is consistent when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqualConsistent() throws N4MException {
			assertFalse(unequalMessage.equals(testMessage));
			assertFalse(unequalMessage.equals(testMessage));
		}

		/**
		 * Tests that equals is consistent when the objects are equal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualConsistent() throws N4MException {
			assertEquals(equalMessage.equals(testMessage),
					equalMessage.equals(testMessage));

		}

		/**
		 * Test that x.equals(null) returns false
		 */
		@Test
		void testEqualsObjectEqualNull() {
			assertFalse(testMessage.equals(null));
		}
	}
	
	
}
