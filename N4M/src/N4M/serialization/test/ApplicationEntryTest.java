/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import N4M.serialization.ApplicationEntry;
import N4M.serialization.N4MException;

/**
 * Tests the ApplicationEntry class
 * @author Clint Masters
 *
 */
class ApplicationEntryTest {
	// The app used for testing
	private ApplicationEntry testApp;
	// Name during tests
	private static final String TESTNAME = "testName";
	// Count for tests
	private static final int TESTCOUNT = 1;
	
	/**
	 * Set up for tests
	 * @throws N4MException if validation fails
	 */
	ApplicationEntryTest() throws N4MException{
		testApp = new ApplicationEntry(TESTNAME,TESTCOUNT);
	}

	/**
	 * Tests a valid constructor
	 * @throws N4MException if validation fails
	 */
	@Test
	void testConstructorValid() throws N4MException {
		String name = "testName";
		int count = 1;
		ApplicationEntry testAppCon = new ApplicationEntry(name,count);
		assertEquals(name,testAppCon.getApplicationName());
		assertEquals(count,testAppCon.getAccessCount());
	}

	/**
	 * Tests the toString method
	 */
	@Test
	void testToStringValid() {
		assertEquals("Name: "+TESTNAME+"\tTimes Accessed: "+TESTCOUNT,testApp.toString());
	}

	/**
	 * Tests getting the access count
	 */
	@Test
	void testGetAccessCount() {
		assertEquals(TESTCOUNT, testApp.getAccessCount());
	}

	/**
	 * Tests setting the access count
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetAccessCountValid() throws N4MException {
		int testCt = 5;
		testApp.setAccessCount(testCt);
		assertEquals(testCt,testApp.getAccessCount());
	}
	
	/**
	 * Tests setting bad access count
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetAccessCountInvalid() throws N4MException {
		assertThrows(N4MException.class, ()->{
			int testCt = 69000;
			testApp.setAccessCount(testCt);
		});
	}

	/**
	 * test getting application name
	 */
	@Test
	void testGetApplicationName() {
		assertEquals(TESTNAME,testApp.getApplicationName());
	}

	/**
	 * Test setting application name
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetApplicationNameValid() throws N4MException {
		String testName = "SetName";
		testApp.setApplicationName(testName);
		assertEquals(testName,testApp.getApplicationName());
	}

	/**
	 * Tests setting bad application name
	 * @throws N4MException if validation fails
	 */
	@Test
	void testSetApplicationNameInvalid() throws N4MException {
		assertThrows(N4MException.class, ()->{
			String testName = "";
			for(int i =0;i<260;i++) {
				testName+=i+"";
			}
			testApp.setApplicationName(testName);
		});
	}

	/************************************************
	 * Tests for the hashCode method
	 ***********************************************/

	/**
	 * Tests that the hashCode function works on the same object
	 */
	@Test
	void testHashCodeSameObject() {
		assertEquals(testApp.hashCode(),testApp.hashCode(),
				"Did not return same hashcode for same object");
	}

	/**
	 * Test that the hashCode function works on equal objects
	 * @throws N4MException 
	 */
	@Test
	void testHashCodeEqualObject() throws N4MException {
		ApplicationEntry hashApp = new ApplicationEntry(TESTNAME,TESTCOUNT);
		assertEquals(testApp.hashCode(),hashApp.hashCode(),
				"Did not return same hashcode for equal objects");
	}

	@DisplayName("Equals Tests")
	@Nested
	class EqualsTests {
		/*********************************************
		 * Tests for the Equals method
		 ********************************************/

		/**
		 * Tests the equal method for when the objects have the same ApplicationEntry
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqual() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry(TESTNAME,TESTCOUNT);
			assertTrue(testApp.equals(equalApp1));
		}

		/**
		 * Tests that the equals method works when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqual() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry("BADNAME",7);
			assertFalse(testApp.equals(equalApp1));
		}

		/**
		 * Tests the reflexive property of the equals method which is
		 * that x.equals(x) should return true
		 */
		@Test
		void testEqualsObjectEqualReflexive() {
			assertTrue(testApp.equals(testApp));
		}

		/**
		 * Tests the symmetric property of equals which is that if x.equals(y)
		 * is true then y.equals(x) should be true
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualSymmetric() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry(TESTNAME,TESTCOUNT);
			assertTrue(equalApp1.equals(testApp));
			assertTrue(testApp.equals(equalApp1));
		}

		/**
		 * Tests the transitive property of equals which is that if x.equals(y)
		 * and y.equals(z) then x.equals(z)
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualTransitive() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry(TESTNAME,TESTCOUNT);
			ApplicationEntry equalApp2 = new ApplicationEntry(TESTNAME,TESTCOUNT);
			assertTrue(equalApp1.equals(equalApp2));
			assertTrue(equalApp2.equals(testApp));
			assertTrue(equalApp1.equals(testApp));
		}

		/**
		 * Tests that equals is consistent when the objects are unequal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectUnEqualConsistent() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry("UnequalName",7);
			assertFalse(equalApp1.equals(testApp));
			assertFalse(equalApp1.equals(testApp));
		}

		/**
		 * Tests that equals is consistent when the objects are equal
		 * @throws N4MException 
		 */
		@Test
		void testEqualsObjectEqualConsistent() throws N4MException {
			ApplicationEntry equalApp1 = new ApplicationEntry(TESTNAME,TESTCOUNT);
			assertEquals(equalApp1.equals(testApp),
					equalApp1.equals(testApp));

		}

		/**
		 * Test that x.equals(null) returns false
		 */
		@Test
		void testEqualsObjectEqualNull() {
			assertFalse(testApp.equals(null));
		}
	}
}
