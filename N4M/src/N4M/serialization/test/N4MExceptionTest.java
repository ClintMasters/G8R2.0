/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import N4M.serialization.ErrorCodeType;
import N4M.serialization.N4MException;

/**
 * Tests the N4MException class
 * @author Clint Masters
 *
 */
class N4MExceptionTest {
	
	/**
	 * Tests creating a N4MException without a cause
	 */
	@Test
	void testValidConstructorWithoutCause() {
		N4MException testException = new N4MException("Test",ErrorCodeType.NOERROR);
		assertEquals(ErrorCodeType.NOERROR,testException.getErrorCodeType());
	}
	
	/**
	 * Tests creating a N4MException with a cause
	 */
	@Test
	void testValidConstructorCause() {
		N4MException testException = new N4MException("Test",ErrorCodeType.NOERROR,
				new Throwable("TESTTHROW"));
		assertEquals(ErrorCodeType.NOERROR,testException.getErrorCodeType());
	}
	

}
