/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents an N4M response and provides serialization/deserialization
 * @version 1.0
 * 
 */
public class N4MResponse extends N4MMessage {
	// Header for the response
	private static final byte HEADER = 0b00101000;
	// Offset to the Application count in a frame
	private static final int COUNTOFFSET = 4;
	// Offset to the application entries in byte array
	private static final int APPOFFSET = 5;
	// Maximum int size for unsigned
	private static final long MAXINTSIZE = 4294967295L;
	// Maximum size of an unsigned byte
	private static final int MAXBYTESIZE = 255;
	// List of applications in the response
	private List<ApplicationEntry> applications = new ArrayList<>();
	// Date of the last time an application was accessed
	private Date timestamp;

	/**
	 * Creates a new N4M response using given values
	 * @param errorCodeNum error number
	 * @param msgId message ID
	 * @param timestamp timestamp
	 * @param applications list of applications
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if timestamp or applications is null
	 */
	public N4MResponse(int errorCodeNum,int msgId,Date timestamp,
			List<ApplicationEntry> applications) throws N4MException{
		setErrorCodeNum(errorCodeNum);
		setMsgId(msgId);
		setApplications(applications);
		setTimestamp(timestamp);
	}

	/**
	 * Create new N4MResponse from byte array
	 * @param in byte array
	 * @param msgId message ID
	 * @param errorCodeNum error number
	 * @throws N4MException if validation fails
	 */
	protected N4MResponse(byte[] in, int msgId, int errorCodeNum) throws N4MException {
		setMsgId(msgId);
		setErrorCodeNum(errorCodeNum);
		setAppListFromBytes(in);
		long seconds = getSecondsFromBytes(in);
		setTimestamp(new Date(seconds*1000));

	}

	/**
	 * Encodes a N4M response to a byte array
	 * @return byte array of N4MResponse
	 */
	@Override
	public byte[] encode() {
		byte header = (byte) (HEADER | (byte)getErrorCodeNum());
		List<Byte> outList = new ArrayList<>();
		outList.add(header);
		outList.add((byte)getMsgId());
		setBytesFromSeconds(outList);
		setBytesFromAppList(outList);
		byte[] retArr = new byte[outList.size()];
		for(int i = 0;i<retArr.length;i++) {
			retArr[i]=outList.get(i);
		}
		return retArr;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		String retStr = "";
		retStr+=super.toString();
		retStr+="\nTimestamp: "+getTimestamp();
		retStr+="\nApplications:\n";
		for(ApplicationEntry a : applications) {
			retStr+=a;
		}
		return retStr;
	}

	/**
	 * Get list of applications
	 * @return list of applications
	 */
	public List<ApplicationEntry> getApplications() {
		return applications.stream().map(ApplicationEntry::new).collect(Collectors.toList());
	}

	/**
	 * Set the list of applications
	 * @param applications list of applications
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if applications is null
	 */
	public void setApplications(List<ApplicationEntry> applications) throws N4MException {
		this.applications=Objects.requireNonNull(applications,"Applications "
				+ "cannot be null").stream().map(ApplicationEntry::new).collect(Collectors.toList());
		if(applications.size()>MAXBYTESIZE) {
			throw new N4MException("Too many applications, must be less than 255", 
					ErrorCodeType.BADMSGSIZE);
		}
	}

	/**
	 * Return timestamp
	 * @return timestamp
	 */
	public Date getTimestamp() {
		return (Date)timestamp.clone();
	}

	/**
	 * Set timestamp
	 * @param timestamp response timestamp
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if timestamp is null
	 */
	public void setTimestamp(Date timestamp) throws N4MException {
		this.timestamp = (Date)Objects.requireNonNull(timestamp, "Timestamp cannot be null").clone();
		if(timestamp.getTime()<0||((timestamp.getTime()/1000)>MAXINTSIZE)) {
			throw new N4MException("Invalid time",
					ErrorCodeType.BADMSG);
		}

		boolean notAccessed=true;
		for(int i = 0;i<applications.size()&&notAccessed;i++) {
			if(applications.get(i).getAccessCount()>0) {
				notAccessed=false;
			}
		}
		if(notAccessed&&timestamp.getTime()!=0) {
			throw new N4MException("Time must be 0 if no applications have been accessed",
					ErrorCodeType.BADMSG);
		}

	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(),getTimestamp(),
				getApplications());
	}

	@Override
	public boolean equals(Object obj) {
		boolean equality = false;
		// Tests if same object
		if(this==obj) {
			equality = true;
		}
		// Tests if same type of object
		else if(!(obj instanceof N4MResponse)) {
			equality = false;
		}
		// Tests if have same parts
		else {
			N4MResponse other = (N4MResponse)obj;
			if(super.equals(other)
					&& getTimestamp().equals(other.getTimestamp())
					&& getApplications().equals(other.getApplications())) {
				equality = true;
			}
		}
		return equality;
	}

	/**
	 * Gets the seconds for a timestamp from byte array
	 * @param in byte array containing N4MResponse
	 * @return
	 */
	private long getSecondsFromBytes(byte[] in) {
		long seconds = Byte.toUnsignedLong(in[0])<<24;
		seconds += Byte.toUnsignedLong(in[1])<<16;
		seconds += Byte.toUnsignedLong(in[2])<<8;
		seconds += Byte.toUnsignedLong(in[3]);
		return seconds;
	}

	/**
	 * Sets the bytes for a N4MResponse timestamp
	 * @param outList the list of bytes
	 */
	private void setBytesFromSeconds(List<Byte> outList) {
		long seconds = getTimestamp().getTime()/1000;
		outList.add((byte) (seconds>>>24));
		outList.add((byte) (seconds>>>16));
		outList.add((byte) (seconds>>>8));
		outList.add((byte) (seconds));
	}

	/**
	 * Sets the application list from a byte array
	 * @param in byte array containing a N4MResponse
	 * @throws N4MException if validation fails
	 */
	private void setAppListFromBytes(byte[] in) throws N4MException{
		int appCount = Byte.toUnsignedInt(in[COUNTOFFSET]);
		int offset = APPOFFSET;
		try {
			for(int i = 0; i<appCount;i++) {
				int useCount=Byte.toUnsignedInt(in[offset++]);
				useCount=useCount<<8;
				useCount+=Byte.toUnsignedInt(in[offset++]);
				int appNameLength = Byte.toUnsignedInt(in[offset++]);
				String appName = "";
				try {
					for(int j = 0; j<appNameLength;j++) {
						appName+=(char)in[offset++];
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					throw new N4MException("Incorrect application name: not enough characters",
							ErrorCodeType.BADMSGSIZE);
				}
				applications.add(new ApplicationEntry(appName,useCount));
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new N4MException("Incorrect application count: too few applications",
					ErrorCodeType.BADMSGSIZE);
		}
	}

	/**
	 * Sets the bytes for the application list
	 * @param outList the list of bytes containing the response
	 */
	private void setBytesFromAppList(List<Byte> outList) {
		outList.add((byte)getApplications().size());
		for(ApplicationEntry a : getApplications()) {
			String appName = a.getApplicationName();
			outList.add((byte)(a.getAccessCount()>>>8));
			outList.add((byte)a.getAccessCount());
			outList.add((byte)appName.length());
			for(int i=0;i<appName.length();i++) {
				outList.add((byte)appName.charAt(i));
			}
		}
	}
}
