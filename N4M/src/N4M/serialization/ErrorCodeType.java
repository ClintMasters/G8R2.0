/************************************************
 *
 * Author: Clint Masters
 * Assignment: Program 4
 * Class: CSI 4321
 *
 ************************************************/
package N4M.serialization;

/**
 * Error Code enumerated type
 * @version 1.0
 *
 */
public enum ErrorCodeType {
	NOERROR(0),
	INCORRECTHEADER(1),
	BADMSGSIZE(2),
	BADMSG(3),
	SYSTEMERROR(4);
	
	// the errorCodeNum for the ErrorCodeType
	private final int errorCodeNum;
	
	
	ErrorCodeType(int errorCodeNum){
		this.errorCodeNum=errorCodeNum;
	}
	
	/**
	 * Return error code number corresponding to the error code
	 * @return error code number
	 */
	public int getErrorCodeNum() {
		return this.errorCodeNum;
	}
	
	/**
	 * Return error code corresponding to the error code number
	 * @param errorCodeNum error code number to find
	 * @return corresponding error code
	 * @throws N4MException if invalid error code number
	 */
	public static ErrorCodeType valueOf(int errorCodeNum) throws N4MException {
		for(ErrorCodeType c : ErrorCodeType.values()) {
			if(c.getErrorCodeNum()==errorCodeNum) {
				return c;
			}
		}
		throw new N4MException("Invalid error code number",SYSTEMERROR);
	}
	
}
